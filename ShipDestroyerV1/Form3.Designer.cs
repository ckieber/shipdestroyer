namespace ShipDestroyerV1
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.richTextBox_background = new System.Windows.Forms.RichTextBox();
            this.button_backgroundOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox_background
            // 
            this.richTextBox_background.BackColor = System.Drawing.Color.Wheat;
            this.richTextBox_background.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_background.Location = new System.Drawing.Point(12, 12);
            this.richTextBox_background.Name = "richTextBox_background";
            this.richTextBox_background.ReadOnly = true;
            this.richTextBox_background.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox_background.Size = new System.Drawing.Size(428, 192);
            this.richTextBox_background.TabIndex = 0;
            this.richTextBox_background.Text = resources.GetString("richTextBox_background.Text");
            // 
            // button_backgroundOk
            // 
            this.button_backgroundOk.BackColor = System.Drawing.Color.Wheat;
            this.button_backgroundOk.Location = new System.Drawing.Point(357, 210);
            this.button_backgroundOk.Name = "button_backgroundOk";
            this.button_backgroundOk.Size = new System.Drawing.Size(83, 31);
            this.button_backgroundOk.TabIndex = 1;
            this.button_backgroundOk.Text = "OK";
            this.button_backgroundOk.UseVisualStyleBackColor = false;
            this.button_backgroundOk.Click += new System.EventHandler(this.button_backgroundOk_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(444, 244);
            this.Controls.Add(this.button_backgroundOk);
            this.Controls.Add(this.richTextBox_background);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(460, 280);
            this.MinimumSize = new System.Drawing.Size(460, 280);
            this.Name = "Form3";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Background Information";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox_background;
        private System.Windows.Forms.Button button_backgroundOk;

    }
}