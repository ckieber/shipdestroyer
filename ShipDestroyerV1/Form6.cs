using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ShipDestroyerV1
{
    public partial class Form6 : Form
    {
        public Form2 form2;
        private Color _oldDestroyed;
        private Color _oldHit;
        private Color _oldMark;
        private Color _oldShip;
        private Color _oldWater;
        private Color _oldMiss;

        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            panel_destroyed.BackColor = form2._destroyedColor;
            panel_hit.BackColor = form2._hitColor;
            panel_mark.BackColor = form2._markField;
            panel_ship.BackColor = form2._shipColor;
            panel_water.BackColor = form2._waterColor;
            panel_miss.BackColor = form2._missColor;

            _oldDestroyed = form2._destroyedColor;
            _oldHit = form2._hitColor;
            _oldMark = form2._markField;
            _oldShip = form2._shipColor;
            _oldWater = form2._waterColor;
            _oldMiss = form2._missColor;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            form2._waterColor = panel_water.BackColor;
            form2._shipColor = panel_ship.BackColor;
            form2._destroyedColor = panel_destroyed.BackColor;
            form2._hitColor = panel_hit.BackColor;
            form2._markField = panel_mark.BackColor;
            form2._missColor = panel_miss.BackColor;

            refreshColors();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_water.BackColor = colorDialog1.Color;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_ship.BackColor = colorDialog1.Color;         
        }

        private void button3_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_destroyed.BackColor = colorDialog1.Color;    
        }

        private void button4_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_hit.BackColor = colorDialog1.Color;    
        }

        private void button5_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_mark.BackColor = colorDialog1.Color;    
        }

        private void button6_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel_miss.BackColor = colorDialog1.Color;  
        }

        private void refreshColors()
        {
            Color color;

            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    color = form2._panelMatrix1[row, col].BackColor;
                    if (color == _oldWater)
                    {
                        form2._panelMatrix1[row, col].BackColor = panel_water.BackColor;
                    }
                    else if (color == _oldHit)
                    {
                        form2._panelMatrix1[row, col].BackColor = panel_hit.BackColor;
                    }
                    else if (color == _oldMiss)
                    {
                        form2._panelMatrix1[row, col].BackColor = panel_miss.BackColor;
                    }
                    else if (color == _oldShip)
                    {
                        form2._panelMatrix1[row, col].BackColor = panel_ship.BackColor;
                    }
                    else if (color == _oldDestroyed)
                    {
                        form2._panelMatrix1[row, col].BackColor = panel_destroyed.BackColor;
                    }
                    //
                    // for panel 2
                    //
                    color = form2._panelMatrix2[row, col].BackColor;
                    if (color == _oldWater)
                    {
                        form2._panelMatrix2[row, col].BackColor = panel_water.BackColor;
                    }
                    else if (color == _oldHit)
                    {
                        form2._panelMatrix2[row, col].BackColor = panel_hit.BackColor;
                    }
                    else if (color == _oldMiss)
                    {
                        form2._panelMatrix2[row, col].BackColor = panel_miss.BackColor;
                    }
                    else if (color == _oldShip)
                    {
                        form2._panelMatrix2[row, col].BackColor = panel_ship.BackColor;
                    }
                    else if (color == _oldDestroyed)
                    {
                        form2._panelMatrix2[row, col].BackColor = panel_destroyed.BackColor;
                    }
                    else if (color == _oldMark)
                    {
                        form2._panelMatrix2[row, col].BackColor = panel_mark.BackColor;
                    }
                }
            }
        }

        private void button_default_Click(object sender, EventArgs e)
        {
            panel_water.BackColor = form2._defaultColors[0];
            panel_ship.BackColor = form2._defaultColors[1];
            panel_miss.BackColor = form2._defaultColors[2];
            panel_hit.BackColor = form2._defaultColors[3];
            panel_mark.BackColor = form2._defaultColors[4];
            panel_destroyed.BackColor = form2._defaultColors[5];
        }
    }
}