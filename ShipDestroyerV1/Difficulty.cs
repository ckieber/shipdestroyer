using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ShipDestroyerV1
{
    [Serializable]
    public class Difficulty
    {
        private int _wins;
        private int _losses;
        private int _total;
        private double _winPercent;
        private double _losePercent;
        private DateTime _fastWinTime;
        private DateTime _fastLoseTime;
        private int _highestScore;

        public Difficulty()
        {
            _wins = 0;
            _losses = 0;
            _total = 0;
            _winPercent = 0;
            _losePercent = 0;
            _fastWinTime = DateTime.MaxValue;
            _fastLoseTime = DateTime.MaxValue;
            _highestScore = 0;
        }

        public void addWin(DateTime gameTime)
        {
            int newScore = Form2.calculateScore();
            _wins++;
            _total++;

            if (gameTime.CompareTo(_fastWinTime) < 0)
            {
                _fastWinTime = gameTime;
            }

            _winPercent = ((100.0 / _total) * _wins);
            _losePercent = 100 - _winPercent;

            if (_highestScore < newScore)
            {
                _highestScore = newScore;
            }
        }

        public void addLoss(DateTime gameTime)
        {
            _losses++;
            _total++;

            if (gameTime.CompareTo(_fastLoseTime) < 0)
            {
                _fastLoseTime = gameTime;
            }

            _winPercent = ((100.0 / _total) * _wins);            
            _losePercent = 100 - _winPercent;
        }

        public int getWins()
        {
            return _wins;
        }

        public int getLosses()
        {
            return _losses;
        }

        public double getWinPercent()
        {
            return _winPercent;
        }

        public double getLosePercent()
        {
            return _losePercent;
        }

        public DateTime getFastWinTime()
        {
            return _fastWinTime;
        }

        public DateTime getFastLoseTime()
        {
            return _fastLoseTime;
        }

        public int getScore()
        {
            return _highestScore;
        }
    }
}
