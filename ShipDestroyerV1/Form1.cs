using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace ShipDestroyerV1
{
    public partial class Form1 : Form
    {
        public string _playerName;
        public int _pageNumber = 1;
        public char _difficulty;
        public double _coinToss; //1...player, 2...computer
        private System.Threading.Thread _newThread;
        public Point _centerScreenLocation;

        //page1 components
        private Label label_title;
        private Label label_nameEnter;
        private TextBox textBox_name;

        //page2 components
        private RadioButton radioButton_difficultyEasy;
        private RadioButton radioButton_difficultyHard;
        private RadioButton radioButton_difficultyImpossible;
        private Label label_title2;

        //page3 components
        private Label label_coinTossTitle;
        private Label label_coinTossResult;
        private ProgressBar progressBar_coinToss;

        public Form1()
        {
            InitializeComponent();
            page1();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_newThread != null)
            {
                _newThread.Abort();
            }
        }

        private delegate void UpdateHandler();

        private delegate void UpdateHandlerChangeText(string text);

        private delegate void UpdateHandlerProgressBar(int value);

        private void changeTextToss(string text)
        {
            label_coinTossResult.Text = text;
        }

        private void progressBarMax(int max)
        {
            progressBar_coinToss.Maximum = max;
        }

        private void progressBarMin(int min)
        {
            progressBar_coinToss.Minimum = min;
        }

        private void progressBarValue(int value)
        {
            progressBar_coinToss.Value = value;
        }

        private void enableButtonNext()
        {
            button_next.Enabled = true;
        }

        private void button_next_Click(object sender, EventArgs e)
        {
            if (_pageNumber < 4)
            {
                _pageNumber++;
                clearPage();

                switch (Char.Parse(_pageNumber.ToString()))
                {
                    case '2':
                        _playerName = changeName(textBox_name.Text);
                        page2();
                        break;
                    case '3':
                        page3();
                        break;
                    case '4':
                        Form2 form2 = new Form2();
                        form2.form1 = this;
                        this.Hide();
                        form2.Show();
                        break;
                    default:
                        break;
                }
            }
        }
        
        private void button_previous_Click(object sender, EventArgs e)
        {
            if (_pageNumber > 1)
            {
                _pageNumber--;
                clearPage();

                switch (Char.Parse(_pageNumber.ToString()))
                {
                    case '1':
                        page1();
                        break;
                    case '2':
                        page2();
                        break;
                    default:
                        break;
                }
            }
        }

        public void clearPage()
        {
            panel_pages.Controls.Clear();
        }

        public void page1()
        {
            label_title = new Label();
            label_nameEnter = new Label();
            textBox_name = new TextBox();
            // 
            // label_title
            // 
            label_title.AutoSize = true;
            label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label_title.Location = new System.Drawing.Point(34, 25);
            label_title.Name = "label_title";
            label_title.Size = new System.Drawing.Size(343, 31);
            label_title.TabIndex = 0;
            label_title.Text = "Welcome to Ship Destroyer";
            // 
            // label_nameEnter
            // 
            label_nameEnter.AutoSize = true;
            label_nameEnter.Location = new System.Drawing.Point(40, 116);
            label_nameEnter.Name = "label_nameEnter";
            label_nameEnter.Size = new System.Drawing.Size(124, 13);
            label_nameEnter.TabIndex = 1;
            label_nameEnter.Text = "Please enter your name (max 9 characters): ";
            // 
            // textBox_name
            // 
            textBox_name.Location = new System.Drawing.Point(260, 113);
            textBox_name.Name = "textBox_name";
            textBox_name.Size = new System.Drawing.Size(115, 20);
            textBox_name.TabIndex = 2;
            textBox_name.TextChanged += new System.EventHandler(textBox_name_TextChanged);
            textBox_name.TabIndex = 1;

            //add to the panel
            panel_pages.Controls.Add(label_title);
            panel_pages.Controls.Add(label_nameEnter);
            panel_pages.Controls.Add(textBox_name);

            button_next.Text = "Next";
            button_next.Enabled = false;
            button_previous.Enabled = false;

            button_next.TabIndex = 2;
            button_previous.TabIndex = 3;
        }

        public void page2()
        {
            radioButton_difficultyEasy = new RadioButton();
            radioButton_difficultyHard = new RadioButton();
            radioButton_difficultyImpossible = new RadioButton();
            label_title2 = new Label();
            // 
            // label_title2
            // 
            label_title2.AutoSize = true;
            label_title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label_title2.Location = new System.Drawing.Point(0, 38);
            label_title2.Name = "label_title";
            label_title2.Size = new System.Drawing.Size(327, 25);
            label_title2.TabIndex = 1;
            label_title2.Text = _playerName + " please choose the difficulty level";
            // 
            // radioButton_difficultyEasy
            // 
            radioButton_difficultyEasy.AutoSize = true;
            radioButton_difficultyEasy.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radioButton_difficultyEasy.Location = new System.Drawing.Point(151, 100);
            radioButton_difficultyEasy.Name = "radioButton_difficultyEasy";
            radioButton_difficultyEasy.Size = new System.Drawing.Size(74, 29);
            radioButton_difficultyEasy.TabIndex = 10;
            radioButton_difficultyEasy.TabStop = false;
            radioButton_difficultyEasy.Text = "Easy";
            radioButton_difficultyEasy.UseVisualStyleBackColor = true;
            radioButton_difficultyEasy.CheckedChanged += new System.EventHandler(radioButton_difficultyEasy_CheckedChanged);
            radioButton_difficultyEasy.Checked = false;
            // 
            // radioButton_difficultyHard
            // 
            radioButton_difficultyHard.AutoSize = true;
            radioButton_difficultyHard.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radioButton_difficultyHard.Location = new System.Drawing.Point(151, 135);
            radioButton_difficultyHard.Name = "radioButton_difficultyHard";
            radioButton_difficultyHard.Size = new System.Drawing.Size(72, 29);
            radioButton_difficultyHard.TabIndex = 11;
            radioButton_difficultyHard.TabStop = false;
            radioButton_difficultyHard.Text = "Hard";
            radioButton_difficultyHard.UseVisualStyleBackColor = true;
            radioButton_difficultyHard.CheckedChanged += new System.EventHandler(radioButton_difficultyHard_CheckedChanged);
            radioButton_difficultyHard.Checked = true;
            // 
            // radioButton_difficultyImpossible
            // 
            radioButton_difficultyImpossible.AutoSize = true;
            radioButton_difficultyImpossible.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radioButton_difficultyImpossible.Location = new System.Drawing.Point(151, 170);
            radioButton_difficultyImpossible.Name = "radioButton_difficultyImpossible";
            radioButton_difficultyImpossible.Size = new System.Drawing.Size(123, 29);
            radioButton_difficultyImpossible.TabIndex = 12;
            radioButton_difficultyImpossible.TabStop = false;
            radioButton_difficultyImpossible.Text = "Impossible";
            radioButton_difficultyImpossible.UseVisualStyleBackColor = true;
            radioButton_difficultyImpossible.CheckedChanged += new System.EventHandler(radioButton_difficultyImpossible_CheckedChanged);
            radioButton_difficultyImpossible.Checked = false;

            //add to panel
            panel_pages.Controls.Add(label_title2);
            panel_pages.Controls.Add(radioButton_difficultyEasy);
            panel_pages.Controls.Add(radioButton_difficultyHard);
            panel_pages.Controls.Add(radioButton_difficultyImpossible);

            button_previous.Enabled = true;
            button_next.Enabled = true;

            button_next.TabIndex = 3;
            button_previous.TabIndex = 4;
        }

        private void page3()
        {
            label_coinTossTitle = new Label();
            label_coinTossResult = new Label();
            progressBar_coinToss = new ProgressBar();
            button_toss = new Button();
            // 
            // label_coinTossTitle
            // 
            label_coinTossTitle.AutoSize = true;
            label_coinTossTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label_coinTossTitle.Location = new System.Drawing.Point(150, 32);
            label_coinTossTitle.Name = "label_coinTossTitle";
            label_coinTossTitle.Size = new System.Drawing.Size(94, 25);
            label_coinTossTitle.TabIndex = 0;
            label_coinTossTitle.Text = "Coin toss";
            // 
            // label_coinTossResult
            // 
            label_coinTossResult.AutoSize = true;
            label_coinTossResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label_coinTossResult.Location = new System.Drawing.Point(70, 128);
            label_coinTossResult.Name = "label_coinTossResult";
            label_coinTossResult.Size = new System.Drawing.Size(0, 25);
            label_coinTossResult.TabIndex = 2;
            // 
            // progressBar_coinToss
            // 
            progressBar_coinToss.Location = new System.Drawing.Point(155, 130);
            progressBar_coinToss.Name = "progressBar_coinToss";
            progressBar_coinToss.Size = new System.Drawing.Size(89, 23);
            progressBar_coinToss.TabIndex = 1;
            progressBar_coinToss.BackColor = Color.Wheat;
            // 
            // button_toss
            // 
            button_toss.Location = new System.Drawing.Point(174, 214);
            button_toss.Name = "button_toss";
            button_toss.Size = new System.Drawing.Size(67, 34);
            button_toss.TabIndex = 0;
            button_toss.Text = "Toss!";
            button_toss.UseVisualStyleBackColor = true;
            button_toss.Click += new System.EventHandler(this.button_toss_Click);
            button_toss.BackColor = Color.Wheat;
            button_toss.TabIndex = 1;

            //add to panel
            panel_pages.Controls.Add(label_coinTossTitle);
            panel_pages.Controls.Add(label_coinTossResult);
            panel_pages.Controls.Add(progressBar_coinToss);
            panel_pages.Controls.Add(button_toss);

            progressBar_coinToss.Hide();
            label_coinTossResult.Hide();

            button_next.Enabled = false;
            button_previous.Enabled = false;
            button_next.Text = "Start";

            button_next.TabIndex = 2;
            button_previous.TabIndex = 3;
        }

        private double calculateCoinToss()
        {
            Random rand = new Random();
            int sleepTime = 500;
            Invoke(new UpdateHandlerProgressBar(progressBarMin), 0);
            Invoke(new UpdateHandlerProgressBar(progressBarMax), 10);
            //progressBar_coinToss.Maximum = 10;
            //progressBar_coinToss.Minimum = 0;

            for (int i = 0; i < rand.Next(20); i++)
            {
                if (progressBar_coinToss.Value >= 10)
                {
                    Invoke(new UpdateHandlerProgressBar(progressBarValue), 0);
                    //progressBar_coinToss.Value = 0;
                }
                Invoke(new UpdateHandlerProgressBar(progressBar_coinToss.Increment), 1);
                //progressBar_coinToss.Increment(1);
                System.Threading.Thread.Sleep(sleepTime);
            }

            return rand.Next(1, 3);
        }

        private void radioButton_difficultyEasy_CheckedChanged(object sender, EventArgs e)
        {
            _difficulty = '1';
        }

        private void radioButton_difficultyHard_CheckedChanged(object sender, EventArgs e)
        {
            _difficulty = '2';
        }

        private void radioButton_difficultyImpossible_CheckedChanged(object sender, EventArgs e)
        {
            _difficulty = '3';
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonTossThread()
        {
            Invoke(new UpdateHandler(button_toss.Hide));
            //button_toss.Hide();
            Invoke(new UpdateHandler(progressBar_coinToss.Show));
            //progressBar_coinToss.Show();
            double tossResult = calculateCoinToss();
            _coinToss = tossResult;
            if (tossResult == 1)
            {
                Invoke(new UpdateHandlerChangeText(changeTextToss), "      You won the coin toss!");
                //label_coinTossResult.Text = "      You won the coin toss!";
            }
            else
                Invoke(new UpdateHandlerChangeText(changeTextToss), "The computer won the coin toss!");
                //label_coinTossResult.Text = "The computer won the coin toss!";
            Invoke(new UpdateHandler(label_coinTossResult.Show));
            //label_coinTossResult.Show();

            Invoke(new UpdateHandler(enableButtonNext));
            //button_next.Enabled = true;
        }

        private void button_toss_Click(object sender, EventArgs e)
        {
            _newThread = new System.Threading.Thread(buttonTossThread);
            _newThread.Start();
        }

        private void textBox_name_TextChanged(object sender, EventArgs e)
        {
            if (textBox_name.Text == "" || textBox_name.Text.Length >= 10)
                button_next.Enabled = false;
            else
                button_next.Enabled = true;
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 background = new Form3();
            background.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 rules = new Form4();
            rules.ShowDialog();
        }

        private string changeName(string name)
        {
            string firstLetter = name.Substring(0, 1).ToUpper();
            string rest = name.Substring(1).ToLower();

            return firstLetter + rest;            
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //settings for the open box
            openFileDialog_start.Filter = "Ship Destroyer File (.shd)|*.shd";
            openFileDialog_start.CheckPathExists = true;
            openFileDialog_start.AddExtension = true;
            openFileDialog_start.DefaultExt = "shd";
            openFileDialog_start.RestoreDirectory = false;
            openFileDialog_start.ValidateNames = true;
            openFileDialog_start.ShowDialog();
        }

        private void openFileDialog_start_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = openFileDialog_start.FileName;
            Form2 form2 = new Form2();
            form2.form1 = this;
            clearPage();

            if (fileName != "")
            {
                try
                {
                    Stream stream = File.Open(fileName, FileMode.Open);
                    BinaryFormatter formatter = new BinaryFormatter();
                    FileInformation info = (FileInformation)formatter.Deserialize(stream);
                    stream.Close();
                    string color = "";

                    _playerName = info._playerName;
                    _difficulty = info._difficulty;
                    form2._targetListHistory = info._targetListHistory;
                    form2._computerHitHistory = info._computerHitHistory;
                    form2._computerTargetListHistory = info._computerTargetListHistory;
                    form2._phase = info._phase;
                    form2._numberOfShips = info._numberOfShips;
                    form2._computerNumberOfShips = info._computerNumberOfShips;
                    form2._lifeAircraftCarrier = info._lifeAircraftCarrier;
                    form2._lifeBattleship = info._lifeBattleship;
                    form2._lifeDestroyer = info._lifeDestroyer;
                    form2._lifePatrolBoat = info._lifePatrolBoat;
                    form2._lifeSubmarine = info._lifeSubmarine;
                    form2._computerLifeAircraftCarrier = info._computerLifeAircraftCarrier;
                    form2._computerLifeBattleship = info._computerLifeBattleship;
                    form2._computerLifeDestroyer = info._computerLifeDestroyer;
                    form2._computerLifePatrolBoat = info._computerLifePatrolBoat;
                    form2._computerLifeSubmarine = info._computerLifeSubmarine;
                    form2._shipMatrix1 = info._shipMatrix1;
                    form2._shipMatrix2 = info._shipMatrix2;
                    form2.textBox_history.Text = info._historyTextBox;
                    //order: water, ship, miss, hit, mark, destroyed
                    form2._waterColor = Color.FromArgb(info._colors[0]);
                    form2._shipColor = Color.FromArgb(info._colors[1]);
                    form2._missColor = Color.FromArgb(info._colors[2]);
                    form2._hitColor = Color.FromArgb(info._colors[3]);
                    form2._markField = Color.FromArgb(info._colors[4]);
                    form2._destroyedColor = Color.FromArgb(info._colors[5]);
                    for (int row = 0; row < 10; row++)
                    {
                        for (int col = 0; col < 10; col++)
                        {
                            color = info.getPanelMatrix1(row, col);
                            if (color.Equals("water"))
                            {
                                form2._panelMatrix1[row, col].BackColor = form2._waterColor;
                            }
                            else if (color.Equals("hit"))
                            {
                                form2._panelMatrix1[row, col].BackColor = form2._hitColor;
                            }
                            else if (color.Equals("miss"))
                            {
                                form2._panelMatrix1[row, col].BackColor = form2._missColor;
                            }
                            else if (color.Equals("ship"))
                            {
                                form2._panelMatrix1[row, col].BackColor = form2._shipColor;
                            }
                            else if (color.Equals("destroyed"))
                            {
                                form2._panelMatrix1[row, col].BackColor = form2._destroyedColor;
                            }

                            // for panel 2
                            color = info.getPanelMatrix2(row, col);
                            if (color.Equals("water"))
                            {
                                form2._panelMatrix2[row, col].BackColor = form2._waterColor;
                            }
                            else if (color.Equals("hit"))
                            {
                                form2._panelMatrix2[row, col].BackColor = form2._hitColor;
                            }
                            else if (color.Equals("miss"))
                            {
                                form2._panelMatrix2[row, col].BackColor = form2._missColor;
                            }
                            else if (color.Equals("ship"))
                            {
                                form2._panelMatrix2[row, col].BackColor = form2._shipColor;
                            }
                            else if (color.Equals("destroyed"))
                            {
                                form2._panelMatrix2[row, col].BackColor = form2._destroyedColor;
                            }
                        }
                    }

                    //everything is loaded
                    form2._fileLoaded = true;
                    form2._finishedGame = false;
                    form2._saveAsMenuItem.Enabled = true;
                    form2.activateTextBoxHistory();
                    form2.textBox_history.SelectionStart = form2.textBox_history.Text.Length;
                    form2.textBox_history.ScrollToCaret();
                    form2.label_playerName.Text = _playerName;
                    form2._targetList.Clear();
                    form2.textBox_targetList.Clear();

                    if (form2._phase == 1) //player is shooter
                    {
                        form2.disablePanels1();
                        form2.enablePanels2();

                        form2.label_comments.Text = _playerName + " please select your target list";
                    }
                    else //computer is shooter
                    {
                        form2.disablePanels1();
                        form2.disablePanels2();
                        form2._newThreadComputerShoot = new Thread(form2.computerShoot);
                        form2._newThreadComputerShoot.Start();
                        //computerShoot();
                    }
                    this.Hide();
                    form2.Show();
                    form2.Focus();
                }
                catch
                {
                    MessageBox.Show("Error occured while loading the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5();
            form5.ShowDialog();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            panel_buttons.Refresh();
            panel_pages.Refresh();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            _centerScreenLocation = new Point(Location.X, Location.Y);
        }
    }
}