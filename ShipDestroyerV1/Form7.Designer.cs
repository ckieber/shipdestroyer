namespace ShipDestroyerV1
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form7));
            this.label_easyWin = new System.Windows.Forms.Label();
            this.label_easyWinPercent = new System.Windows.Forms.Label();
            this.label_easyLoss = new System.Windows.Forms.Label();
            this.label_easyLossPercent = new System.Windows.Forms.Label();
            this.label_easyFastTimeWin = new System.Windows.Forms.Label();
            this.label_easyFastTimeLoss = new System.Windows.Forms.Label();
            this.label_hardFastTimeLoss = new System.Windows.Forms.Label();
            this.label_hardFastTimeWin = new System.Windows.Forms.Label();
            this.label_hardLossPercent = new System.Windows.Forms.Label();
            this.label_hardLoss = new System.Windows.Forms.Label();
            this.label_hardWinPercent = new System.Windows.Forms.Label();
            this.label_hardWin = new System.Windows.Forms.Label();
            this.label_impossibleFastTimeLoss = new System.Windows.Forms.Label();
            this.label_impossibleFastTimeWin = new System.Windows.Forms.Label();
            this.label_impossibleLossPercent = new System.Windows.Forms.Label();
            this.label_impossibleLoss = new System.Windows.Forms.Label();
            this.label_impossibleWinPercent = new System.Windows.Forms.Label();
            this.label_impossibleWin = new System.Windows.Forms.Label();
            this.label_playerName = new System.Windows.Forms.Label();
            this.button_clear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label_easyScore = new System.Windows.Forms.Label();
            this.label_hardScore = new System.Windows.Forms.Label();
            this.label_impossibleScore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_easyWin
            // 
            this.label_easyWin.AutoSize = true;
            this.label_easyWin.BackColor = System.Drawing.Color.Transparent;
            this.label_easyWin.Location = new System.Drawing.Point(65, 62);
            this.label_easyWin.Name = "label_easyWin";
            this.label_easyWin.Size = new System.Drawing.Size(35, 13);
            this.label_easyWin.TabIndex = 0;
            this.label_easyWin.Text = "label1";
            // 
            // label_easyWinPercent
            // 
            this.label_easyWinPercent.AutoSize = true;
            this.label_easyWinPercent.BackColor = System.Drawing.Color.Transparent;
            this.label_easyWinPercent.Location = new System.Drawing.Point(106, 62);
            this.label_easyWinPercent.Name = "label_easyWinPercent";
            this.label_easyWinPercent.Size = new System.Drawing.Size(35, 13);
            this.label_easyWinPercent.TabIndex = 1;
            this.label_easyWinPercent.Text = "label1";
            // 
            // label_easyLoss
            // 
            this.label_easyLoss.AutoSize = true;
            this.label_easyLoss.BackColor = System.Drawing.Color.Transparent;
            this.label_easyLoss.Location = new System.Drawing.Point(147, 62);
            this.label_easyLoss.Name = "label_easyLoss";
            this.label_easyLoss.Size = new System.Drawing.Size(35, 13);
            this.label_easyLoss.TabIndex = 2;
            this.label_easyLoss.Text = "label1";
            // 
            // label_easyLossPercent
            // 
            this.label_easyLossPercent.AutoSize = true;
            this.label_easyLossPercent.BackColor = System.Drawing.Color.Transparent;
            this.label_easyLossPercent.Location = new System.Drawing.Point(188, 62);
            this.label_easyLossPercent.Name = "label_easyLossPercent";
            this.label_easyLossPercent.Size = new System.Drawing.Size(35, 13);
            this.label_easyLossPercent.TabIndex = 3;
            this.label_easyLossPercent.Text = "label1";
            // 
            // label_easyFastTimeWin
            // 
            this.label_easyFastTimeWin.AutoSize = true;
            this.label_easyFastTimeWin.BackColor = System.Drawing.Color.Transparent;
            this.label_easyFastTimeWin.Location = new System.Drawing.Point(229, 62);
            this.label_easyFastTimeWin.Name = "label_easyFastTimeWin";
            this.label_easyFastTimeWin.Size = new System.Drawing.Size(35, 13);
            this.label_easyFastTimeWin.TabIndex = 4;
            this.label_easyFastTimeWin.Text = "label1";
            // 
            // label_easyFastTimeLoss
            // 
            this.label_easyFastTimeLoss.AutoSize = true;
            this.label_easyFastTimeLoss.BackColor = System.Drawing.Color.Transparent;
            this.label_easyFastTimeLoss.Location = new System.Drawing.Point(270, 62);
            this.label_easyFastTimeLoss.Name = "label_easyFastTimeLoss";
            this.label_easyFastTimeLoss.Size = new System.Drawing.Size(35, 13);
            this.label_easyFastTimeLoss.TabIndex = 5;
            this.label_easyFastTimeLoss.Text = "label1";
            // 
            // label_hardFastTimeLoss
            // 
            this.label_hardFastTimeLoss.AutoSize = true;
            this.label_hardFastTimeLoss.BackColor = System.Drawing.Color.Transparent;
            this.label_hardFastTimeLoss.Location = new System.Drawing.Point(270, 86);
            this.label_hardFastTimeLoss.Name = "label_hardFastTimeLoss";
            this.label_hardFastTimeLoss.Size = new System.Drawing.Size(35, 13);
            this.label_hardFastTimeLoss.TabIndex = 11;
            this.label_hardFastTimeLoss.Text = "label1";
            // 
            // label_hardFastTimeWin
            // 
            this.label_hardFastTimeWin.AutoSize = true;
            this.label_hardFastTimeWin.BackColor = System.Drawing.Color.Transparent;
            this.label_hardFastTimeWin.Location = new System.Drawing.Point(229, 86);
            this.label_hardFastTimeWin.Name = "label_hardFastTimeWin";
            this.label_hardFastTimeWin.Size = new System.Drawing.Size(35, 13);
            this.label_hardFastTimeWin.TabIndex = 10;
            this.label_hardFastTimeWin.Text = "label1";
            // 
            // label_hardLossPercent
            // 
            this.label_hardLossPercent.AutoSize = true;
            this.label_hardLossPercent.BackColor = System.Drawing.Color.Transparent;
            this.label_hardLossPercent.Location = new System.Drawing.Point(188, 86);
            this.label_hardLossPercent.Name = "label_hardLossPercent";
            this.label_hardLossPercent.Size = new System.Drawing.Size(35, 13);
            this.label_hardLossPercent.TabIndex = 9;
            this.label_hardLossPercent.Text = "label1";
            // 
            // label_hardLoss
            // 
            this.label_hardLoss.AutoSize = true;
            this.label_hardLoss.BackColor = System.Drawing.Color.Transparent;
            this.label_hardLoss.Location = new System.Drawing.Point(147, 86);
            this.label_hardLoss.Name = "label_hardLoss";
            this.label_hardLoss.Size = new System.Drawing.Size(35, 13);
            this.label_hardLoss.TabIndex = 8;
            this.label_hardLoss.Text = "label1";
            // 
            // label_hardWinPercent
            // 
            this.label_hardWinPercent.AutoSize = true;
            this.label_hardWinPercent.BackColor = System.Drawing.Color.Transparent;
            this.label_hardWinPercent.Location = new System.Drawing.Point(106, 86);
            this.label_hardWinPercent.Name = "label_hardWinPercent";
            this.label_hardWinPercent.Size = new System.Drawing.Size(35, 13);
            this.label_hardWinPercent.TabIndex = 7;
            this.label_hardWinPercent.Text = "label1";
            // 
            // label_hardWin
            // 
            this.label_hardWin.AutoSize = true;
            this.label_hardWin.BackColor = System.Drawing.Color.Transparent;
            this.label_hardWin.Location = new System.Drawing.Point(65, 86);
            this.label_hardWin.Name = "label_hardWin";
            this.label_hardWin.Size = new System.Drawing.Size(35, 13);
            this.label_hardWin.TabIndex = 6;
            this.label_hardWin.Text = "label1";
            // 
            // label_impossibleFastTimeLoss
            // 
            this.label_impossibleFastTimeLoss.AutoSize = true;
            this.label_impossibleFastTimeLoss.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleFastTimeLoss.Location = new System.Drawing.Point(270, 112);
            this.label_impossibleFastTimeLoss.Name = "label_impossibleFastTimeLoss";
            this.label_impossibleFastTimeLoss.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleFastTimeLoss.TabIndex = 17;
            this.label_impossibleFastTimeLoss.Text = "label1";
            // 
            // label_impossibleFastTimeWin
            // 
            this.label_impossibleFastTimeWin.AutoSize = true;
            this.label_impossibleFastTimeWin.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleFastTimeWin.Location = new System.Drawing.Point(229, 112);
            this.label_impossibleFastTimeWin.Name = "label_impossibleFastTimeWin";
            this.label_impossibleFastTimeWin.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleFastTimeWin.TabIndex = 16;
            this.label_impossibleFastTimeWin.Text = "label1";
            // 
            // label_impossibleLossPercent
            // 
            this.label_impossibleLossPercent.AutoSize = true;
            this.label_impossibleLossPercent.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleLossPercent.Location = new System.Drawing.Point(188, 112);
            this.label_impossibleLossPercent.Name = "label_impossibleLossPercent";
            this.label_impossibleLossPercent.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleLossPercent.TabIndex = 15;
            this.label_impossibleLossPercent.Text = "label1";
            // 
            // label_impossibleLoss
            // 
            this.label_impossibleLoss.AutoSize = true;
            this.label_impossibleLoss.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleLoss.Location = new System.Drawing.Point(147, 112);
            this.label_impossibleLoss.Name = "label_impossibleLoss";
            this.label_impossibleLoss.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleLoss.TabIndex = 14;
            this.label_impossibleLoss.Text = "label1";
            // 
            // label_impossibleWinPercent
            // 
            this.label_impossibleWinPercent.AutoSize = true;
            this.label_impossibleWinPercent.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleWinPercent.Location = new System.Drawing.Point(106, 112);
            this.label_impossibleWinPercent.Name = "label_impossibleWinPercent";
            this.label_impossibleWinPercent.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleWinPercent.TabIndex = 13;
            this.label_impossibleWinPercent.Text = "label1";
            // 
            // label_impossibleWin
            // 
            this.label_impossibleWin.AutoSize = true;
            this.label_impossibleWin.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleWin.Location = new System.Drawing.Point(65, 112);
            this.label_impossibleWin.Name = "label_impossibleWin";
            this.label_impossibleWin.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleWin.TabIndex = 12;
            this.label_impossibleWin.Text = "label1";
            // 
            // label_playerName
            // 
            this.label_playerName.AutoSize = true;
            this.label_playerName.Location = new System.Drawing.Point(48, 9);
            this.label_playerName.Name = "label_playerName";
            this.label_playerName.Size = new System.Drawing.Size(35, 13);
            this.label_playerName.TabIndex = 18;
            this.label_playerName.Text = "label1";
            // 
            // button_clear
            // 
            this.button_clear.BackColor = System.Drawing.Color.Wheat;
            this.button_clear.Location = new System.Drawing.Point(191, 152);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(83, 31);
            this.button_clear.TabIndex = 2;
            this.button_clear.Text = "Reset";
            this.button_clear.UseVisualStyleBackColor = false;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(65, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Wins";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(147, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Losses";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(106, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Win%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(188, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Lose%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(229, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Fastest";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(229, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Winning";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(229, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Time";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(270, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Time";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(270, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Losing";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(270, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Fastest";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(7, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Easy";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(7, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Hard";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(7, 112);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Impossible";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Wheat;
            this.button7.Location = new System.Drawing.Point(102, 152);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 31);
            this.button7.TabIndex = 1;
            this.button7.Text = "OK";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(319, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "(max=60)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(319, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Score";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(319, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Highest";
            // 
            // label_easyScore
            // 
            this.label_easyScore.AutoSize = true;
            this.label_easyScore.BackColor = System.Drawing.Color.Transparent;
            this.label_easyScore.Location = new System.Drawing.Point(319, 62);
            this.label_easyScore.Name = "label_easyScore";
            this.label_easyScore.Size = new System.Drawing.Size(35, 13);
            this.label_easyScore.TabIndex = 37;
            this.label_easyScore.Text = "label1";
            // 
            // label_hardScore
            // 
            this.label_hardScore.AutoSize = true;
            this.label_hardScore.BackColor = System.Drawing.Color.Transparent;
            this.label_hardScore.Location = new System.Drawing.Point(319, 86);
            this.label_hardScore.Name = "label_hardScore";
            this.label_hardScore.Size = new System.Drawing.Size(35, 13);
            this.label_hardScore.TabIndex = 38;
            this.label_hardScore.Text = "label1";
            // 
            // label_impossibleScore
            // 
            this.label_impossibleScore.AutoSize = true;
            this.label_impossibleScore.BackColor = System.Drawing.Color.Transparent;
            this.label_impossibleScore.Location = new System.Drawing.Point(319, 112);
            this.label_impossibleScore.Name = "label_impossibleScore";
            this.label_impossibleScore.Size = new System.Drawing.Size(35, 13);
            this.label_impossibleScore.TabIndex = 39;
            this.label_impossibleScore.Text = "label1";
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(364, 186);
            this.Controls.Add(this.label_impossibleScore);
            this.Controls.Add(this.label_hardScore);
            this.Controls.Add(this.label_easyScore);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.label_playerName);
            this.Controls.Add(this.label_impossibleFastTimeLoss);
            this.Controls.Add(this.label_impossibleFastTimeWin);
            this.Controls.Add(this.label_impossibleLossPercent);
            this.Controls.Add(this.label_impossibleLoss);
            this.Controls.Add(this.label_impossibleWinPercent);
            this.Controls.Add(this.label_impossibleWin);
            this.Controls.Add(this.label_hardFastTimeLoss);
            this.Controls.Add(this.label_hardFastTimeWin);
            this.Controls.Add(this.label_hardLossPercent);
            this.Controls.Add(this.label_hardLoss);
            this.Controls.Add(this.label_hardWinPercent);
            this.Controls.Add(this.label_hardWin);
            this.Controls.Add(this.label_easyFastTimeLoss);
            this.Controls.Add(this.label_easyFastTimeWin);
            this.Controls.Add(this.label_easyLossPercent);
            this.Controls.Add(this.label_easyLoss);
            this.Controls.Add(this.label_easyWinPercent);
            this.Controls.Add(this.label_easyWin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(380, 222);
            this.MinimumSize = new System.Drawing.Size(380, 222);
            this.Name = "Form7";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form7_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_easyWin;
        private System.Windows.Forms.Label label_easyWinPercent;
        private System.Windows.Forms.Label label_easyLoss;
        private System.Windows.Forms.Label label_easyLossPercent;
        private System.Windows.Forms.Label label_easyFastTimeWin;
        private System.Windows.Forms.Label label_easyFastTimeLoss;
        private System.Windows.Forms.Label label_hardFastTimeLoss;
        private System.Windows.Forms.Label label_hardFastTimeWin;
        private System.Windows.Forms.Label label_hardLossPercent;
        private System.Windows.Forms.Label label_hardLoss;
        private System.Windows.Forms.Label label_hardWinPercent;
        private System.Windows.Forms.Label label_hardWin;
        private System.Windows.Forms.Label label_impossibleFastTimeLoss;
        private System.Windows.Forms.Label label_impossibleFastTimeWin;
        private System.Windows.Forms.Label label_impossibleLossPercent;
        private System.Windows.Forms.Label label_impossibleLoss;
        private System.Windows.Forms.Label label_impossibleWinPercent;
        private System.Windows.Forms.Label label_impossibleWin;
        private System.Windows.Forms.Label label_playerName;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label_easyScore;
        private System.Windows.Forms.Label label_hardScore;
        private System.Windows.Forms.Label label_impossibleScore;

    }
}