using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ShipDestroyerV1
{
    [Serializable]
    public class Record
    {
        private string _name;
        private Difficulty _easy;
        private Difficulty _hard;
        private Difficulty _impossible;

        public Record()
        {
            _name = "";
            _easy = new Difficulty();
            _hard = new Difficulty();
            _impossible = new Difficulty();
        }

        public Record(string name)
        {
            _name = name;
            _easy = new Difficulty();
            _hard = new Difficulty();
            _impossible = new Difficulty();
        }

        public Record(string name, Difficulty easy, Difficulty hard, Difficulty impossible)
        {
            _name = name;
            _easy = easy;
            _hard = hard;
            _impossible = impossible;
        }

        public string getName()
        {
            return _name;
        }

        public Difficulty getDifficulty(char difficulty)
        {
            if (difficulty == '1')
            {
                return _easy;
            }
            else if (difficulty == '2')
            {
                return _hard;
            }
            else if (difficulty == '3')
            {
                return _impossible;
            }
            else
                return null;
        }

        public override bool Equals(object obj)
        {
            return _name.Equals(((Record)obj).getName());
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        public void clear()
        {
            _easy = new Difficulty();
            _hard = new Difficulty();
            _impossible = new Difficulty();
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("name", _name);
            info.AddValue("easy", _easy);
            info.AddValue("hard", _hard);
            info.AddValue("impossible", _impossible);
        }

        public Record(SerializationInfo info, StreamingContext ctxt)
        {
            _name = (string)info.GetValue("name", typeof(string));
            _easy = (Difficulty)info.GetValue("easy", typeof(Difficulty));
            _hard = (Difficulty)info.GetValue("hard", typeof(Difficulty));
            _impossible = (Difficulty)info.GetValue("impossible", typeof(Difficulty));
        }
    }
}
