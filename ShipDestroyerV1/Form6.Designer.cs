namespace ShipDestroyerV1
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form6));
            this.label_shipColor = new System.Windows.Forms.Label();
            this.label_water = new System.Windows.Forms.Label();
            this.label_destroyed = new System.Windows.Forms.Label();
            this.label_hit = new System.Windows.Forms.Label();
            this.label_mark = new System.Windows.Forms.Label();
            this.panel_water = new System.Windows.Forms.Panel();
            this.panel_ship = new System.Windows.Forms.Panel();
            this.panel_destroyed = new System.Windows.Forms.Panel();
            this.panel_hit = new System.Windows.Forms.Panel();
            this.panel_mark = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel_miss = new System.Windows.Forms.Panel();
            this.label_missed = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.button_default = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_shipColor
            // 
            this.label_shipColor.AutoSize = true;
            this.label_shipColor.BackColor = System.Drawing.Color.Transparent;
            this.label_shipColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_shipColor.Location = new System.Drawing.Point(8, 39);
            this.label_shipColor.Name = "label_shipColor";
            this.label_shipColor.Size = new System.Drawing.Size(58, 25);
            this.label_shipColor.TabIndex = 0;
            this.label_shipColor.Text = "Ship:";
            // 
            // label_water
            // 
            this.label_water.AutoSize = true;
            this.label_water.BackColor = System.Drawing.Color.Transparent;
            this.label_water.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_water.Location = new System.Drawing.Point(8, 8);
            this.label_water.Name = "label_water";
            this.label_water.Size = new System.Drawing.Size(71, 25);
            this.label_water.TabIndex = 1;
            this.label_water.Text = "Water:";
            // 
            // label_destroyed
            // 
            this.label_destroyed.AutoSize = true;
            this.label_destroyed.BackColor = System.Drawing.Color.Transparent;
            this.label_destroyed.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_destroyed.Location = new System.Drawing.Point(8, 70);
            this.label_destroyed.Name = "label_destroyed";
            this.label_destroyed.Size = new System.Drawing.Size(148, 25);
            this.label_destroyed.TabIndex = 2;
            this.label_destroyed.Text = "Destroyed ship:";
            // 
            // label_hit
            // 
            this.label_hit.AutoSize = true;
            this.label_hit.BackColor = System.Drawing.Color.Transparent;
            this.label_hit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_hit.Location = new System.Drawing.Point(8, 101);
            this.label_hit.Name = "label_hit";
            this.label_hit.Size = new System.Drawing.Size(82, 25);
            this.label_hit.TabIndex = 3;
            this.label_hit.Text = "Hit ship:";
            // 
            // label_mark
            // 
            this.label_mark.AutoSize = true;
            this.label_mark.BackColor = System.Drawing.Color.Transparent;
            this.label_mark.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_mark.Location = new System.Drawing.Point(8, 132);
            this.label_mark.Name = "label_mark";
            this.label_mark.Size = new System.Drawing.Size(124, 25);
            this.label_mark.TabIndex = 4;
            this.label_mark.Text = "Marked field:";
            // 
            // panel_water
            // 
            this.panel_water.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_water.Location = new System.Drawing.Point(154, 8);
            this.panel_water.Name = "panel_water";
            this.panel_water.Size = new System.Drawing.Size(25, 25);
            this.panel_water.TabIndex = 5;
            // 
            // panel_ship
            // 
            this.panel_ship.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ship.Location = new System.Drawing.Point(154, 39);
            this.panel_ship.Name = "panel_ship";
            this.panel_ship.Size = new System.Drawing.Size(25, 25);
            this.panel_ship.TabIndex = 6;
            // 
            // panel_destroyed
            // 
            this.panel_destroyed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_destroyed.Location = new System.Drawing.Point(154, 70);
            this.panel_destroyed.Name = "panel_destroyed";
            this.panel_destroyed.Size = new System.Drawing.Size(25, 25);
            this.panel_destroyed.TabIndex = 7;
            // 
            // panel_hit
            // 
            this.panel_hit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_hit.Location = new System.Drawing.Point(154, 101);
            this.panel_hit.Name = "panel_hit";
            this.panel_hit.Size = new System.Drawing.Size(25, 25);
            this.panel_hit.TabIndex = 6;
            // 
            // panel_mark
            // 
            this.panel_mark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_mark.Location = new System.Drawing.Point(154, 132);
            this.panel_mark.Name = "panel_mark";
            this.panel_mark.Size = new System.Drawing.Size(25, 25);
            this.panel_mark.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Wheat;
            this.button1.Location = new System.Drawing.Point(204, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 25);
            this.button1.TabIndex = 8;
            this.button1.Text = "change";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Wheat;
            this.button2.Location = new System.Drawing.Point(204, 39);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 25);
            this.button2.TabIndex = 9;
            this.button2.Text = "change";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Wheat;
            this.button3.Location = new System.Drawing.Point(204, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 25);
            this.button3.TabIndex = 10;
            this.button3.Text = "change";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Wheat;
            this.button4.Location = new System.Drawing.Point(204, 101);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(63, 25);
            this.button4.TabIndex = 11;
            this.button4.Text = "change";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Wheat;
            this.button5.Location = new System.Drawing.Point(204, 132);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(63, 25);
            this.button5.TabIndex = 12;
            this.button5.Text = "change";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Wheat;
            this.button7.Location = new System.Drawing.Point(184, 244);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 31);
            this.button7.TabIndex = 13;
            this.button7.Text = "OK";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Wheat;
            this.button6.Location = new System.Drawing.Point(204, 163);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(63, 25);
            this.button6.TabIndex = 16;
            this.button6.Text = "change";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // panel_miss
            // 
            this.panel_miss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_miss.Location = new System.Drawing.Point(154, 163);
            this.panel_miss.Name = "panel_miss";
            this.panel_miss.Size = new System.Drawing.Size(25, 25);
            this.panel_miss.TabIndex = 15;
            // 
            // label_missed
            // 
            this.label_missed.AutoSize = true;
            this.label_missed.BackColor = System.Drawing.Color.Transparent;
            this.label_missed.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_missed.Location = new System.Drawing.Point(8, 163);
            this.label_missed.Name = "label_missed";
            this.label_missed.Size = new System.Drawing.Size(122, 25);
            this.label_missed.TabIndex = 14;
            this.label_missed.Text = "Missed ship:";
            // 
            // button_default
            // 
            this.button_default.BackColor = System.Drawing.Color.Wheat;
            this.button_default.Location = new System.Drawing.Point(86, 204);
            this.button_default.Name = "button_default";
            this.button_default.Size = new System.Drawing.Size(113, 25);
            this.button_default.TabIndex = 17;
            this.button_default.Text = "default colors";
            this.button_default.UseVisualStyleBackColor = false;
            this.button_default.Click += new System.EventHandler(this.button_default_Click);
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(275, 278);
            this.Controls.Add(this.button_default);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.panel_miss);
            this.Controls.Add(this.label_missed);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel_mark);
            this.Controls.Add(this.panel_hit);
            this.Controls.Add(this.panel_destroyed);
            this.Controls.Add(this.panel_ship);
            this.Controls.Add(this.panel_water);
            this.Controls.Add(this.label_mark);
            this.Controls.Add(this.label_hit);
            this.Controls.Add(this.label_destroyed);
            this.Controls.Add(this.label_water);
            this.Controls.Add(this.label_shipColor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(291, 314);
            this.MinimumSize = new System.Drawing.Size(291, 314);
            this.Name = "Form6";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Colors";
            this.Load += new System.EventHandler(this.Form6_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_shipColor;
        private System.Windows.Forms.Label label_water;
        private System.Windows.Forms.Label label_destroyed;
        private System.Windows.Forms.Label label_hit;
        private System.Windows.Forms.Label label_mark;
        private System.Windows.Forms.Panel panel_water;
        private System.Windows.Forms.Panel panel_ship;
        private System.Windows.Forms.Panel panel_destroyed;
        private System.Windows.Forms.Panel panel_hit;
        private System.Windows.Forms.Panel panel_mark;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel_miss;
        private System.Windows.Forms.Label label_missed;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button button_default;
    }
}