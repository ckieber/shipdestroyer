using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ShipDestroyerV1
{
    public partial class Form7 : Form
    {
        public Form2 form2;
        public bool _clearResult;
        private string _break;
        private string _format;

        public Form7()
        {
            InitializeComponent();
            _break = "     --";
            _format = "{0:0.0}";
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            if (form2._playerName[form2._playerName.Length - 1] == 's')
            {
                this.Text = form2._playerName + "'" + " record";
            }
            else
            {
                this.Text = form2._playerName + "'s" + " record";
            }

            updateTable();
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            Form8 messageBox = new Form8();
            messageBox.form7 = this;
            messageBox.ShowDialog();

            if (_clearResult)
            {
                form2._profile.clear();
                updateTable();
            }
        }

        private void updateTable()
        {
            label_playerName.Text = form2._playerName;

            label_easyWin.Text = form2._profile.getDifficulty('1').getWins().ToString();
            label_easyWinPercent.Text = String.Format(_format, form2._profile.getDifficulty('1').getWinPercent());
            label_easyLoss.Text = form2._profile.getDifficulty('1').getLosses().ToString();
            label_easyLossPercent.Text = String.Format(_format, form2._profile.getDifficulty('1').getLosePercent());
            if (form2._profile.getDifficulty('1').getFastWinTime().Year == 9999)
            {
                label_easyFastTimeWin.Text = _break;
            }
            else
            {
                label_easyFastTimeWin.Text = formatString(form2._profile.getDifficulty('1').getFastWinTime());
            }
            if (form2._profile.getDifficulty('1').getFastLoseTime().Year == 9999)
            {
                label_easyFastTimeLoss.Text = _break;
            }
            else
            {
                label_easyFastTimeLoss.Text = formatString(form2._profile.getDifficulty('1').getFastLoseTime());
            }
            label_easyScore.Text = form2._profile.getDifficulty('1').getScore().ToString();

            label_hardWin.Text = form2._profile.getDifficulty('2').getWins().ToString();
            label_hardWinPercent.Text = String.Format(_format, form2._profile.getDifficulty('2').getWinPercent());
            label_hardLoss.Text = form2._profile.getDifficulty('2').getLosses().ToString();
            label_hardLossPercent.Text = String.Format(_format, form2._profile.getDifficulty('2').getLosePercent());
            if (form2._profile.getDifficulty('2').getFastWinTime().Year == 9999)
            {
                label_hardFastTimeWin.Text = _break;
            }
            else
            {
                label_hardFastTimeWin.Text = formatString(form2._profile.getDifficulty('2').getFastWinTime());
            }
            if (form2._profile.getDifficulty('2').getFastLoseTime().Year == 9999)
            {
                label_hardFastTimeLoss.Text = _break;
            }
            else
            {
                label_hardFastTimeLoss.Text = formatString(form2._profile.getDifficulty('2').getFastLoseTime());
            }
            label_hardScore.Text = form2._profile.getDifficulty('2').getScore().ToString();

            label_impossibleWin.Text = form2._profile.getDifficulty('3').getWins().ToString();
            label_impossibleWinPercent.Text = String.Format(_format, form2._profile.getDifficulty('3').getWinPercent());
            label_impossibleLoss.Text = form2._profile.getDifficulty('3').getLosses().ToString();
            label_impossibleLossPercent.Text = String.Format(_format, form2._profile.getDifficulty('3').getLosePercent());
            if (form2._profile.getDifficulty('3').getFastWinTime().Year == 9999)
            {
                label_impossibleFastTimeWin.Text = _break;
            }
            else
            {
                label_impossibleFastTimeWin.Text = formatString(form2._profile.getDifficulty('3').getFastWinTime());
            }
            if (form2._profile.getDifficulty('3').getFastLoseTime().Year == 9999)
            {
                label_impossibleFastTimeLoss.Text = _break;
            }
            else
            {
                label_impossibleFastTimeLoss.Text = formatString(form2._profile.getDifficulty('3').getFastLoseTime());
            }
            label_impossibleScore.Text = form2._profile.getDifficulty('3').getScore().ToString();
        }

        private string formatString(DateTime time)
        {
            string minutes = "", seconds = "";

            if (time.Minute.ToString().Length == 1)
            {
                minutes = "0" + time.Minute.ToString();
            }
            else
            {
                minutes = time.Minute.ToString();
            }
            minutes += "m";

            if (time.Second.ToString().Length == 1)
            {
                seconds = "0" + time.Second.ToString();
            }
            else
            {
                seconds = time.Second.ToString();
            }
            seconds += "s";

            return minutes + seconds;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}