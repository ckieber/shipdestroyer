using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ShipDestroyerV1
{
    public partial class Form9 : Form
    {
        public bool _sameName;
        public bool _cancel;

        public Form9()
        {
            InitializeComponent();
            _cancel = true;
            _sameName = false;
        }

        private void button_yes_Click(object sender, EventArgs e)
        {
            _sameName = true;
            _cancel = false;
            this.Close();
        }

        private void button_no_Click(object sender, EventArgs e)
        {
            _sameName = false;
            _cancel = false;
            this.Close();
        }
    }
}