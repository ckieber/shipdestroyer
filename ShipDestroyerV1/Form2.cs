using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace ShipDestroyerV1
{
    public partial class Form2 : Form
    {
        public Form1 form1;
        private static Form2 form2;
        public TextBox textBox_history;
        private bool _newGame;
        public string[,] _shipMatrix1;
        public string[,] _shipMatrix2;
        public Panel[,] _panelMatrix1;
        public Panel[,] _panelMatrix2;
        public Color _waterColor;
        public Color _shipColor;
        public Color _missColor;
        public Color _hitColor;
        public Color _markField;
        public Color _destroyedColor;
        public Color[] _defaultColors; //order: water, ship, miss, hit, mark, destroyed
        public string _playerName;
        public char _difficulty;
        private double _tossResult;
        private char _horzVert;
        private int _shipSize;
        private string _shipName;
        private bool _placeRemove;
        public int _lifeAircraftCarrier, _lifeBattleship, _lifeDestroyer, _lifePatrolBoat, _lifeSubmarine;
        public int _computerLifeAircraftCarrier, _computerLifeBattleship, _computerLifeDestroyer, _computerLifePatrolBoat, _computerLifeSubmarine;
        public List<string> _targetList;
        public List<string> _targetListHistory;
        public int _phase; //0...placing ships; 1...shooting; 2...get shot at
        public int _numberOfShips;
        public int _computerNumberOfShips;
        public List<string> _computerTargetListHistory;
        public List<string> _computerHitHistory; //for difficulty hard
        private string _break;
        public Thread _newThreadComputerShoot;
        private Thread _newThreadComputerSetShips;
        private Thread _newThreadButtonShoot;
        private List<Record> _records;
        public Record _profile;
        private DateTime _startTime;
        public bool _finishedGame;
        public bool _fileLoaded;
        private bool _fileSaved;
        public ToolStripMenuItem _saveAsMenuItem;

        public Form2()
        {
            InitializeComponent();
            form2 = this;
            _targetList = new List<string>();
            _targetListHistory = new List<string>();
            _computerHitHistory = new List<string>();
            _computerTargetListHistory = new List<string>();
            _records = new List<Record>();
            _newGame = false;
            _defaultColors = new Color[6];
            _defaultColors[0] = Color.Wheat;
            _defaultColors[1] = Color.Peru;
            _defaultColors[2] = Color.DarkBlue;
            _defaultColors[3] = Color.Red;
            _defaultColors[4] = Color.Black;
            _defaultColors[5] = Color.YellowGreen;
            _waterColor = _defaultColors[0];
            _shipColor = _defaultColors[1];
            _missColor = _defaultColors[2];
            _hitColor = _defaultColors[3];
            _markField = _defaultColors[4];
            _destroyedColor = _defaultColors[5];
            _horzVert = 'h';
            _placeRemove = true;
            _phase = 0;
            _numberOfShips = 5;
            _computerNumberOfShips = 5;
            _lifeAircraftCarrier = 5;
            _lifeBattleship = 4;
            _lifeDestroyer = 3;
            _lifePatrolBoat = 2;
            _lifeSubmarine = 3;
            _computerLifeAircraftCarrier = 5;
            _computerLifeBattleship = 4;
            _computerLifeDestroyer = 3;
            _computerLifePatrolBoat = 2;
            _computerLifeSubmarine = 3;
            _break = "----------------------------\r\n";
            _startTime = DateTime.MaxValue;
            _profile = new Record();
            _finishedGame = true;
            _fileLoaded = false;
            _fileSaved = false;
            _saveAsMenuItem = saveAsToolStripMenuItem;

            //set menu
            loadToolStripMenuItem.Enabled = true;
            saveAsToolStripMenuItem.Enabled = false;
            newToolStripMenuItem.Enabled = true;

            //set radiobuttons
            radioButton_place.Select();
            radioButton_aircraftCarrier.Select();

            //set button
            button_battle.Enabled = false;
            button_shoot.Enabled = false;

            //initialize matrix for ships for the player
            _shipMatrix1 = new string[10,10];
            for (int row = 0; row < 10; row++ )
            {
                for (int col = 0; col < 10; col++ )
                {
                    _shipMatrix1[row, col] = "";
                }
            }

            //initialize matrix for ships for the computer
            _shipMatrix2 = new string[10, 10];
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _shipMatrix2[row, col] = "";
                }
            }

            //initialize matrix for panels
            _panelMatrix1 = new Panel[10, 10] {{panel1,panel2,panel3,panel4,panel5,panel6,panel7,panel8,panel9,panel10},
                                               {panel11,panel12,panel13,panel14,panel15,panel16,panel17,panel18,panel19,panel20},
                                               {panel21,panel22,panel23,panel24,panel25,panel26,panel27,panel28,panel29,panel30},
                                               {panel31,panel32,panel33,panel34,panel35,panel36,panel37,panel38,panel39,panel40},
                                               {panel41,panel42,panel43,panel44,panel45,panel46,panel47,panel48,panel49,panel50},
                                               {panel51,panel52,panel53,panel54,panel55,panel56,panel57,panel58,panel59,panel60},
                                               {panel61,panel62,panel63,panel64,panel65,panel66,panel67,panel68,panel69,panel70},
                                               {panel71,panel72,panel73,panel74,panel75,panel76,panel77,panel78,panel79,panel80},
                                               {panel81,panel82,panel83,panel84,panel85,panel86,panel87,panel88,panel89,panel90},
                                               {panel91,panel92,panel93,panel94,panel95,panel96,panel97,panel98,panel99,panel100}};

            _panelMatrix2 = new Panel[10, 10] {{panel101,panel102,panel103,panel104,panel105,panel106,panel107,panel108,panel109,panel110},
                                               {panel111,panel112,panel113,panel114,panel115,panel116,panel117,panel118,panel119,panel120},
                                               {panel121,panel122,panel123,panel124,panel125,panel126,panel127,panel128,panel129,panel130},
                                               {panel131,panel132,panel133,panel134,panel135,panel136,panel137,panel138,panel139,panel140},
                                               {panel141,panel142,panel143,panel144,panel145,panel146,panel147,panel148,panel149,panel150},
                                               {panel151,panel152,panel153,panel154,panel155,panel156,panel157,panel158,panel159,panel160},
                                               {panel161,panel162,panel163,panel164,panel165,panel166,panel167,panel168,panel169,panel170},
                                               {panel171,panel172,panel173,panel174,panel175,panel176,panel177,panel178,panel179,panel180},
                                               {panel181,panel182,panel183,panel184,panel185,panel186,panel187,panel188,panel189,panel190},
                                               {panel191,panel192,panel193,panel194,panel195,panel196,panel197,panel198,panel199,panel200}};

            //set starting colors
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _panelMatrix1[row, col].BackColor = _waterColor;
                    _panelMatrix2[row, col].BackColor = _waterColor;
                }
            }

            // 
            // textBox_history
            // 
            textBox_history = new TextBox();
            textBox_history.Location = new System.Drawing.Point(-2, -2);
            textBox_history.Multiline = true;
            textBox_history.Name = "textBox_history";
            textBox_history.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBox_history.Size = new System.Drawing.Size(165, 254);
            textBox_history.TabIndex = 26;
            textBox_history.ReadOnly = true;
            textBox_history.BackColor = Color.Wheat;

            //enable and disable panels
            enablePanels1();
            disablePanels2();
        }

        private delegate void UpdateHandler(string str, bool add);

        private delegate void UpdateHandlerUpdate();

        private delegate void UpdateHandlerActivateButton(bool state);

        private delegate void UpdateHandlerUpdatePanel(int row, int col);

        private delegate void UpdatehandlerChangeDestroyedColor(string ship);

        private delegate void UpdateHandlerMenuItmes(bool save, bool load, bool newItem);

        public void activateTextBoxHistory()
        {
            panel_infoBox.Controls.Clear();
            panel_infoBox.Controls.Add(textBox_history);
        }

        private void activateButtonShoot(bool state)
        {
            button_shoot.Enabled = state;
        }

        private void changeLabelComments(string text, bool add)
        {
            if (!add)
            {
                label_comments.Text = text;
            }
            else
            {
                label_comments.Text += text;
            }
            label_comments.Update();
        }

        private void updateLabelComments()
        {
            label_comments.Update();
        }

        private void updateTextBoxTargetList()
        {
            textBox_targetList.Update();
        }

        private void changeTextBoxHistory(string text, bool add)
        {
            if (!add)
            {
                textBox_history.Text = text;
            }
            else
            {
                textBox_history.Text += text;
            }
            textBox_history.Update();

        }

        private void changeTextBoxTargetList(string text,bool add)
        {
            if (!add)
            {
                textBox_targetList.Text = text;
            }
            else
            {
                textBox_targetList.Text += text;
            }
            textBox_targetList.Update();
        }

        private void clearTextBoxTargetList()
        {
            textBox_targetList.Clear();
        }

        public void scrollToEndTextBoxHistory()
        {
            textBox_history.SelectionStart = textBox_history.Text.Length;
            textBox_history.ScrollToCaret();
            textBox_history.Update();
        }

        private void changeMenuItems(bool save, bool load, bool newItem)
        {
            saveAsToolStripMenuItem.Enabled = save;
            loadToolStripMenuItem.Enabled = load;
            newToolStripMenuItem.Enabled = newItem;
        }

        private void updatePanel1(int row, int col)
        {
            _panelMatrix1[row, col].Update();
        }

        private void updatePanel2(int row, int col)
        {
            _panelMatrix2[row, col].Update();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_newThreadButtonShoot != null)
            {
                _newThreadButtonShoot.Abort();
            }
            if (_newThreadComputerSetShips != null)
            {
                _newThreadComputerSetShips.Abort();
            }
            if (_newThreadComputerShoot != null)
            {
                _newThreadComputerShoot.Abort();
            }
            
            if (!_newGame)
            {
                form1.Close();
            }

            if (!_finishedGame && !_fileSaved)
            {
                _profile.getDifficulty(_difficulty).addLoss(DateTime.MaxValue);
            }
            //save the list of records to the file
            saveRecordsToFile();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            _playerName = form1._playerName;
            _difficulty = form1._difficulty;
            _tossResult = form1._coinToss;

            //start with placing the ships comment
            label_comments.Text = _playerName + " please place your ships";
            label_playerName.Text = _playerName;

            //load records
            //load the list of records from the file
            loadRecordsFromFile();
            loadProfile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form9 messageBox = new Form9();
            messageBox.ShowDialog();

            if (!messageBox._cancel)
            {
                _newGame = true;
                form1.clearPage();
                if (messageBox._sameName)
                {
                    form1._playerName = _playerName;
                    form1.page2();
                    form1._pageNumber = 2;
                }
                else
                {
                    form1.page1();
                    form1._pageNumber = 1;
                }
                form1.Location = form1._centerScreenLocation;
                this.Close();
                form1.Show();
                form1.Focus();
            } 
        }

        private void backgroundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 background = new Form3();
            background.ShowDialog();
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 rules = new Form4();
            rules.ShowDialog();
        }

        private void setColorsHorz(int row, int col, int size)
        {
            if (size > 1 && col < 9)
            {
                setColorsHorz(row, col + 1, size - 1);
            }
            _panelMatrix1[row, col].BackColor = _shipColor;
        }

        private void resetColorsHorz(int row, int col, int size)
        {
            if (size > 1 && col < 9)
            {
                resetColorsHorz(row, col + 1, size - 1);
            }
            if (_shipMatrix1[row, col] == "")
            {
                _panelMatrix1[row, col].BackColor = _waterColor;
            }
        }

        private void setShipHorz1(int row, int col, int size, string name)
        {
            if (size > 1 && col < 9)
            {
                setShipHorz1(row, col + 1, size - 1, name);
            }
            _shipMatrix1[row, col] = name;

            if (name == "Aircraft Carrier")
            {
                radioButton_aircraftCarrier.Enabled = false;
                radioButton_aircraftCarrier.Checked = false;

                if(radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                }
                
            }
            else if(name == "Battleship")
            {
                radioButton_battleship.Enabled = false;
                radioButton_battleship.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
            else if (name == "Destroyer")
            {
                radioButton_destroyer.Enabled = false;
                radioButton_destroyer.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
            else if (name == "Patrol Boat")
            {
                radioButton_patrolBoat.Enabled = false;
                radioButton_patrolBoat.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
            else if (name == "Submarine")
            {
                radioButton_submarine.Enabled = false;
                radioButton_submarine.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
        }

        private void setShipHorz2(int row, int col, int size, string name)
        {
            if (size > 1 && col < 9)
            {
                setShipHorz2(row, col + 1, size - 1, name);
            }
            _shipMatrix2[row, col] = name;
        }

        public void disablePanels1()
        {
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _panelMatrix1[row, col].Enabled = false;
                }
            }
        }

        public void disablePanels2()
        {
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _panelMatrix2[row, col].Enabled = false;
                }
            }
        }

        public void enablePanels1()
        {
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _panelMatrix1[row, col].Enabled = true;
                }
            }
        }

        public void enablePanels2()
        {
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _panelMatrix2[row, col].Enabled = true;
                }
            }
        }

        private void setColorsVert(int row, int col, int size)
        {
            if (size > 1 && row < 9)
            {
                setColorsVert(row + 1, col, size - 1);
            }
            _panelMatrix1[row, col].BackColor = _shipColor;
        }

        private void resetColorsVert(int row, int col, int size)
        {
            if (size > 1 && row < 9)
            {
                resetColorsVert(row + 1, col, size - 1);
            }
            if (_shipMatrix1[row, col] == "")
            {
                _panelMatrix1[row, col].BackColor = _waterColor;
            }
        }

        private void setShipVert1(int row, int col, int size, string name)
        {
            if (size > 1 && row < 9)
            {
                setShipVert1(row + 1, col, size - 1, name);
            }
            _shipMatrix1[row, col] = name;

            if (name == "Aircraft Carrier")
            {
                radioButton_aircraftCarrier.Enabled = false;
                radioButton_aircraftCarrier.Checked = false;

                if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                }

            }
            else if (name == "Battleship")
            {
                radioButton_battleship.Enabled = false;
                radioButton_battleship.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
            else if (name == "Destroyer")
            {
                radioButton_destroyer.Enabled = false;
                radioButton_destroyer.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
            else if (name == "Patrol Boat")
            {
                radioButton_patrolBoat.Enabled = false;
                radioButton_patrolBoat.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_submarine.Enabled == true)
                {
                    radioButton_submarine.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
            else if (name == "Submarine")
            {
                radioButton_submarine.Enabled = false;
                radioButton_submarine.Checked = false;

                if (radioButton_aircraftCarrier.Enabled == true)
                {
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (radioButton_battleship.Enabled == true)
                {
                    radioButton_battleship.Checked = true;
                }
                else if (radioButton_destroyer.Enabled == true)
                {
                    radioButton_destroyer.Checked = true;
                }
                else if (radioButton_patrolBoat.Enabled == true)
                {
                    radioButton_patrolBoat.Checked = true;
                }
                else
                {
                    button_battle.Enabled = true;
                    radioButton_place.Enabled = false;
                    radioButton_place.Checked = false;
                    radioButton_remove.Checked = true;
                    /*
                    button_battle.Enabled = true;
                    disablePanels1();
                    radioButton_place.Enabled = false;
                    radioButton_remove.Enabled = false;
                    */
                }
            }
        }

        private void setShipVert2(int row, int col, int size, string name)
        {
            if (size > 1 && row < 9)
            {
                setShipVert2(row + 1, col, size - 1, name);
            }
            _shipMatrix2[row, col] = name;
        }

        private void Form2_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (_horzVert == 'h')
                {
                    _horzVert = 'v';
                }
                else
                {
                    _horzVert = 'h';
                }
            }
        }

        private string helpError(int row, int col, int size)
        {
            string errorMessage = "";

            //check if horizontal or vertical ship goes over the border
            if (_horzVert == 'h' && (col + (size - 1) >= 10))
            {
                errorMessage = "The ship is too big to place it at this position.";
            }
            else if (_horzVert == 'v' && (row + (size - 1)) >= 10)
            {
                errorMessage = "The ship is too big to place it at this position.";
            }
            else//check if there is one empty square between this and another ship
            {
                if (row == 0)
                {
                    if (col == 0)
                    {
                        if (_shipMatrix1[0, 1] != "" || _shipMatrix1[1, 0] != "" || _shipMatrix1[1, 1] != "")
                        {
                            errorMessage = "This ship is too close to another ship.";
                        }
                    }
                    else if (col == 9)
                    {
                        if (_shipMatrix1[0, 8] != "" || _shipMatrix1[1, 8] != "" || _shipMatrix1[1, 9] != "")
                        {
                            errorMessage = "This ship is too close to another ship.";
                        }
                    }
                    else
                    {
                        bool found = false;
                        for (int i = row; i <= row + 1; i++)
                        {
                            for (int j = col - 1; j <= col + 1; j++)
                            {
                                if (_shipMatrix1[i, j] != "")
                                {
                                    found = true;
                                }
                            }
                        }

                        if (found)
                        {
                            errorMessage = "This ship is too close to another ship.";
                        }
                    }
                }
                else if (row == 9)
                {
                    if (col == 0)
                    {
                        if (_shipMatrix1[8, 0] != "" || _shipMatrix1[8, 1] != "" || _shipMatrix1[9, 1] != "")
                        {
                            errorMessage = "This ship is too close to another ship.";
                        }
                    }
                    else if (col == 9)
                    {
                        if (_shipMatrix1[9, 8] != "" || _shipMatrix1[8, 8] != "" || _shipMatrix1[8, 9] != "")
                        {
                            errorMessage = "This ship is too close to another ship.";
                        }
                    }
                    else
                    {
                        bool found = false;
                        for (int i = row - 1; i <= row; i++)
                        {
                            for (int j = col - 1; j <= col + 1; j++)
                            {
                                if (_shipMatrix1[i, j] != "")
                                {
                                    found = true;
                                }
                            }
                        }

                        if (found)
                        {
                            errorMessage = "This ship is too close to another ship.";
                        }
                    }
                }
                else if (col == 0)
                {
                    bool found = false;
                    for (int i = row - 1; i <= row + 1; i++)
                    {
                        for (int j = col; j <= col + 1; j++)
                        {
                            if (_shipMatrix1[i, j] != "")
                            {
                                found = true;
                            }
                        }
                    }

                    if (found)
                    {
                        errorMessage = "This ship is too close to another ship.";
                    }
                }
                else if (col == 9)
                {
                    bool found = false;
                    for (int i = row - 1; i <= row + 1; i++)
                    {
                        for (int j = col - 1; j <= col; j++)
                        {
                            if (_shipMatrix1[i, j] != "")
                            {
                                found = true;
                            }
                        }
                    }

                    if (found)
                    {
                        errorMessage = "This ship is too close to another ship.";
                    }
                }
                else //anywhere else in the field
                {
                    bool found = false;
                    for (int i = row - 1; i <= row + 1; i++)
                    {
                        for (int j = col - 1; j <= col + 1; j++)
                        {
                            if (_shipMatrix1[i, j] != "")
                            {
                                found = true;
                            }
                        }
                    }

                    if (found)
                    {
                        errorMessage = "This ship is too close to another ship.";
                    }
                }
            }

            //check for each field of the ship horizontal
            if (size > 1 && col < 9 && _horzVert == 'h' && errorMessage == "")
            {
                errorMessage = checkIfValidShipPlace(row, col + 1, size - 1);
            }
            //check for each field of the ship vertical
            if (size > 1 && row < 9 && _horzVert == 'v' && errorMessage == "")
            {
                errorMessage = checkIfValidShipPlace(row + 1, col, size - 1);
            }

            return errorMessage;
        }

        private string checkIfValidShipPlace(int row, int col, int size)
        {
            string errorMessage = "";

            //check if there is already a ship
            if (_shipMatrix1[row, col] != "")
            {
                errorMessage = "There is already a ship at this position. Please choose another location";
            }
            else if (_horzVert == 'h' && !((col + 1 >= 10)))
            {
                if (_shipMatrix1[row, col + 1] != "" && size > 1)
                {
                    errorMessage = "There is already a ship at this position. Please choose another location";
                }
                else
                    errorMessage = helpError(row, col, size);
            }
            else if (_horzVert == 'v' && !((row + 1 >= 10)))
            {
                if (_shipMatrix1[row + 1, col] != "" && size > 1)
                {
                    errorMessage = "There is already a ship at this position. Please choose another location";
                }
                else
                    errorMessage = helpError(row, col, size);
            }
            else
            {
                errorMessage = helpError(row, col, size);
            }

            return errorMessage;
        }

        private bool helpError2(int row, int col, int size)
        {
            bool error = false;

            //check if horizontal or vertical ship goes over the border
            if (_horzVert == 'h' && (col + (size - 1) >= 10))
            {
                error = true;
            }
            else if (_horzVert == 'v' && (row + (size - 1)) >= 10)
            {
                error = true;
            }
            else//check if there is one empty square between this and another ship
            {
                if (row == 0)
                {
                    if (col == 0)
                    {
                        if (_shipMatrix2[0, 1] != "" || _shipMatrix2[1, 0] != "" || _shipMatrix2[1, 1] != "")
                        {
                            error = true;
                        }
                    }
                    else if (col == 9)
                    {
                        if (_shipMatrix2[0, 8] != "" || _shipMatrix2[1, 8] != "" || _shipMatrix2[1, 9] != "")
                        {
                            error = true;
                        }
                    }
                    else
                    {
                        bool found = false;
                        for (int i = row; i <= row + 1; i++)
                        {
                            for (int j = col - 1; j <= col + 1; j++)
                            {
                                if (_shipMatrix2[i, j] != "")
                                {
                                    found = true;
                                }
                            }
                        }

                        if (found)
                        {
                            error = true;
                        }
                    }
                }
                else if (row == 9)
                {
                    if (col == 0)
                    {
                        if (_shipMatrix2[8, 0] != "" || _shipMatrix2[8, 1] != "" || _shipMatrix2[9, 1] != "")
                        {
                            error = true;
                        }
                    }
                    else if (col == 9)
                    {
                        if (_shipMatrix2[9, 8] != "" || _shipMatrix2[8, 8] != "" || _shipMatrix2[8, 9] != "")
                        {
                            error = true;
                        }
                    }
                    else
                    {
                        bool found = false;
                        for (int i = row - 1; i <= row; i++)
                        {
                            for (int j = col - 1; j <= col + 1; j++)
                            {
                                if (_shipMatrix2[i, j] != "")
                                {
                                    found = true;
                                }
                            }
                        }

                        if (found)
                        {
                            error = true;
                        }
                    }
                }
                else if (col == 0)
                {
                    bool found = false;
                    for (int i = row - 1; i <= row + 1; i++)
                    {
                        for (int j = col; j <= col + 1; j++)
                        {
                            if (_shipMatrix2[i, j] != "")
                            {
                                found = true;
                            }
                        }
                    }

                    if (found)
                    {
                        error = true;
                    }
                }
                else if (col == 9)
                {
                    bool found = false;
                    for (int i = row - 1; i <= row + 1; i++)
                    {
                        for (int j = col - 1; j <= col; j++)
                        {
                            if (_shipMatrix2[i, j] != "")
                            {
                                found = true;
                            }
                        }
                    }

                    if (found)
                    {
                        error = true;
                    }
                }
                else //anywhere else in the field
                {
                    bool found = false;
                    for (int i = row - 1; i <= row + 1; i++)
                    {
                        for (int j = col - 1; j <= col + 1; j++)
                        {
                            if (_shipMatrix2[i, j] != "")
                            {
                                found = true;
                            }
                        }
                    }

                    if (found)
                    {
                        error = true;
                    }
                }
            }

            //check for each field of the ship horizontal
            if (size > 1 && col < 9 && _horzVert == 'h' && !error)
            {
                error = checkIfValidShipPlace2(row, col + 1, size - 1);
            }
            //check for each field of the ship vertical
            if (size > 1 && row < 9 && _horzVert == 'v' && !error)
            {
                error = checkIfValidShipPlace2(row + 1, col, size - 1);
            }

            return error;
        }

        private bool checkIfValidShipPlace2(int row, int col, int size)
        {
            bool error = false;

            //check if there is already a ship
            if (_shipMatrix2[row, col] != "")
            {
                error = true;
            }
            else if (_horzVert == 'h' && !((col + 1 >= 10)))
            {
                if (_shipMatrix2[row, col + 1] != "" && size > 1)
                {
                    error = true;
                }
                else
                    error = helpError2(row, col, size);
            }
            else if (_horzVert == 'v' && !((row + 1 >= 10)))
            {
                if (_shipMatrix2[row + 1, col] != "" && size > 1)
                {
                    error = true;
                }
                else
                    error = helpError2(row, col, size);
            }
            else
            {
                error = helpError2(row, col, size);
            }

            return error;
        }

        private void radioButton_aircraftCarrier_CheckedChanged(object sender, EventArgs e)
        {
            _shipSize = 5;
            _shipName = "Aircraft Carrier";
        }

        private void radioButton_battleship_CheckedChanged(object sender, EventArgs e)
        {
            _shipSize = 4;
            _shipName = "Battleship";
        }

        private void radioButton_destroyer_CheckedChanged(object sender, EventArgs e)
        {
            _shipSize = 3;
            _shipName = "Destroyer";
        }

        private void radioButton_patrolBoat_CheckedChanged(object sender, EventArgs e)
        {
            _shipSize = 2;
            _shipName = "Patrol Boat";
        }

        private void radioButton_submarine_CheckedChanged(object sender, EventArgs e)
        {
            _shipSize = 3;
            _shipName = "Submarine";
        }

        private void button_battle_Click(object sender, EventArgs e)
        {
            _finishedGame = false;

            //activate menu
            loadToolStripMenuItem.Enabled = false;
            saveAsToolStripMenuItem.Enabled = false;
            newToolStripMenuItem.Enabled = false;

            panel_infoBox.Controls.Clear();
            panel_infoBox.Controls.Add(textBox_history);

            textBox_history.Text = getTime() + ":\r\n";
            _startTime = DateTime.Now.ToLocalTime();
            textBox_history.Text += "Game started!\r\n";
            textBox_history.Update();

            if (_tossResult == 1)
            {
                _phase = 1;
                disablePanels1();
                enablePanels2();
            }
            else
            {
                _phase = 2;
                disablePanels1();
                disablePanels2();
            }
            _newThreadComputerSetShips = new Thread(computerSetShips);
            _newThreadComputerSetShips.Start();
            //computerSetShips();
        }

        private void historyUpdater(int row, int col, string shooter, int events, string ship)
        {
            /* events:
             * 0 == water
             * 1 == hit
             * 2 == ship destroyed
             * 
             * shooter:
             * you
             * the computer
             */

            char rowLetter = ' ';
            switch(row){
                case 0:
                    rowLetter = 'A';
                    break;
                case 1:
                    rowLetter = 'B';
                    break;
                case 2:
                    rowLetter = 'C';
                    break;
                case 3:
                    rowLetter = 'D';
                    break;
                case 4:
                    rowLetter = 'E';
                    break;
                case 5:
                    rowLetter = 'F';
                    break;
                case 6:
                    rowLetter = 'G';
                    break;
                case 7:
                    rowLetter = 'H';
                    break;
                case 8:
                    rowLetter = 'I';
                    break;
                case 9:
                    rowLetter = 'J';
                    break;
            }

            //textBox_history.Text += getTime() + ":\r\n";
            Invoke(new UpdateHandler(changeTextBoxHistory), getTime() + ":\r\n", true);

            if (events == 0)
            {
               // textBox_history.Text += shooter + " shot at " + rowLetter + col + " and missed!\r\n";
                Invoke(new UpdateHandler(changeTextBoxHistory), shooter + " shot at " + rowLetter + col + " and missed!\r\n", true);
            }
            else if (events == 1)
            {
                //textBox_history.Text += shooter + " shot at " + rowLetter + col + " and hit the " + ship + "\r\n";
                Invoke(new UpdateHandler(changeTextBoxHistory), shooter + " shot at " + rowLetter + col + " and hit the " + ship + "\r\n", true);
            }
            else
            {
                //textBox_history.Text += shooter + " shot at " + rowLetter + col + " and sunk the " + ship + "\r\n";
                Invoke(new UpdateHandler(changeTextBoxHistory), shooter + " shot at " + rowLetter + col + " and sunk the " + ship + "\r\n", true);
            }
            if(_computerNumberOfShips == 0)
            {
                //textBox_history.Text += _playerName + " won the battle!!!";
                //label_comments.Text = _playerName + " won the battle!!!";
                Invoke(new UpdateHandler(changeTextBoxHistory), _playerName + " won the battle!!!", true);
                Invoke(new UpdateHandler(changeLabelComments), _playerName + " won the battle!!!", false);             
            }
            else if(_numberOfShips == 0)
            {
                //textBox_history.Text += "The computer won the battle!!!";
                //label_comments.Text = "The computer won the battle!!!";
                Invoke(new UpdateHandler(changeTextBoxHistory), "The computer won the battle!!!", true);
                Invoke(new UpdateHandler(changeLabelComments), "The computer won the battle!!!", false);     
            }
            Invoke(new UpdateHandlerUpdate(scrollToEndTextBoxHistory));
            Invoke(new UpdateHandlerUpdate(updateLabelComments));

            //textBox_history.SelectionStart = textBox_history.Text.Length;
            //textBox_history.ScrollToCaret();
            //textBox_history.Update();
            //label_comments.Update();
        }

        private string getTime()
        {
            DateTime time = DateTime.Now;
            return time.ToLocalTime().ToLongTimeString();
        }

        private void changeShipColorDestroyed(string ship)
        {
            int row = 0, col = 0;
            for (row = 0; row < 10; row++)
            {
                for (col = 0; col < 10; col++)
                {
                    if (_shipMatrix2[row, col].Equals(ship))
                    {
                        _panelMatrix2[row, col].BackColor = _destroyedColor;
                        _panelMatrix2[row, col].Update();
                    }
                }
            }
        }

        private void changeShipColorDestroyed2(string ship)
        {
            int row=0, col=0;
            for (row = 0; row < 10; row++)
            {
                for (col = 0; col < 10; col++)
                {
                    if (_shipMatrix1[row, col].Equals(ship))
                    {
                        _panelMatrix1[row, col].BackColor = _destroyedColor;
                        Invoke(new UpdateHandlerUpdatePanel(updatePanel1), row, col);
                        //_panelMatrix1[row, col].Update();
                    }
                }
            }
        }

        private void helpShoot(int row, int col, string shooter, string ship, int lifeCount)
        {
            if (lifeCount == 0)
            {
                _computerNumberOfShips--;
                historyUpdater(row, col, shooter, 2, ship);
                _panelMatrix2[row, col].BackColor = _hitColor;
                //_panelMatrix2[row, col].Update();
                Invoke(new UpdateHandlerUpdatePanel(updatePanel2), row, col);
                Invoke(new UpdatehandlerChangeDestroyedColor(changeShipColorDestroyed), ship);
                //changeShipColorDestroyed(ship);
            }
            else
            {
                historyUpdater(row, col, shooter, 1, ship);
                _panelMatrix2[row, col].BackColor = _hitColor;
                //_panelMatrix2[row, col].Update();
                Invoke(new UpdateHandlerUpdatePanel(updatePanel2), row, col);
            }
            
        }

        private void helpShoot2(int row, int col, string shooter, string ship, int lifeCount)
        {
            if (lifeCount == 0)
            {
                _numberOfShips--;
                historyUpdater(row, col, shooter, 2, ship);
                _panelMatrix1[row, col].BackColor = _hitColor;
                Invoke(new UpdateHandlerUpdatePanel(updatePanel1), row, col);
                //_panelMatrix1[row, col].Update();
                Invoke(new UpdatehandlerChangeDestroyedColor(changeShipColorDestroyed2), ship);
                //changeShipColorDestroyed2(ship);
                //added
                if (_difficulty == '1' || _difficulty == '2')
                {
                    addFieldsAroundShipToTargetListHistory(ship);
                }
            }
            else
            {
                historyUpdater(row, col, shooter, 1, ship);
                _panelMatrix1[row, col].BackColor = _hitColor;
                Invoke(new UpdateHandlerUpdatePanel(updatePanel1), row, col);
                //_panelMatrix1[row, col].Update();
            }

        }

        private void buttonShootThread()
        {
            int sleepTime = 1000;
            bool done = false;

            Invoke(new UpdateHandlerMenuItmes(changeMenuItems), false, false, false);
            //saveAsToolStripMenuItem.Enabled = false;
            //loadToolStripMenuItem.Enabled = false;
            //newToolStripMenuItem.Enabled = false;

            //change phase to get shot at
            _phase = 2;
            Invoke(new UpdateHandlerActivateButton(activateButtonShoot), false);
            //button_shoot.Enabled = false;
            Invoke(new UpdateHandlerUpdate(disablePanels2));
            //disablePanels2();
            Invoke(new UpdateHandler(changeLabelComments), "Shooting...", false);
            //label_comments.Text = "Shooting...";
            //label_comments.Update();
            //textBox_history.Text += _break;
            Invoke(new UpdateHandler(changeTextBoxHistory), _break, true);

            //check the fields
            while (_targetList.Count != 0 && !done)
            {
                string field = _targetList[0];
                //deactivate the panel
                _panelMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())].Enabled = false;

                if (_shipMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "")
                {
                    historyUpdater(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), _playerName, 0, "");
                    _panelMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())].BackColor = _missColor;
                    Invoke(new UpdateHandlerUpdatePanel(updatePanel2), Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
                    //_panelMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())].Update();
                }
                else //hit
                {
                    //check if the ship was destroyed
                    if (_shipMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Aircraft Carrier")
                    {
                        helpShoot(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), _playerName, "Aircraft Carrier", --_computerLifeAircraftCarrier);
                    }
                    else if (_shipMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Battleship")
                    {
                        helpShoot(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), _playerName, "Battleship", --_computerLifeBattleship);
                    }
                    else if (_shipMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Destroyer")
                    {
                        helpShoot(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), _playerName, "Destroyer", --_computerLifeDestroyer);
                    }
                    else if (_shipMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Patrol Boat")
                    {
                        helpShoot(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), _playerName, "Patrol Boat", --_computerLifePatrolBoat);
                    }
                    else if (_shipMatrix2[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Submarine")
                    {
                        helpShoot(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), _playerName, "Submarine", --_computerLifeSubmarine);
                    }
                }
                _targetListHistory.Add(_targetList[0]);
                _targetList.RemoveAt(0);

                //removing the first line from the target list textbox
                string all = textBox_targetList.Text;
                string rest = all.Substring(textBox_targetList.Lines[0].Length + 2);
                Invoke(new UpdateHandlerUpdate(clearTextBoxTargetList));
                Invoke(new UpdateHandler(changeTextBoxTargetList), rest, false);
                //textBox_targetList.Clear();
                //textBox_targetList.Text = rest;

                //updating textboxes
                Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
                //textBox_targetList.Update();

                //wait a little bit
                System.Threading.Thread.Sleep(sleepTime);
                if (_computerNumberOfShips == 0)
                {
                    done = true;
                }
            }
            if (!done)
            {
                _newThreadComputerShoot = new Thread(computerShoot);
                _newThreadComputerShoot.Start();
                //computerShoot();
            }
            else
            {
                Invoke(new UpdateHandlerMenuItmes(changeMenuItems), saveAsToolStripMenuItem.Enabled, true, true);
                //loadToolStripMenuItem.Enabled = true;
                //newToolStripMenuItem.Enabled = true;

                //update the record
                updateRecord("win");
                _finishedGame = true;

                endOfGameWindow(true);
            }
        }

        private void button_shoot_Click(object sender, EventArgs e)
        {
            _newThreadButtonShoot = new Thread(buttonShootThread);
            _newThreadButtonShoot.Start();
        }

        private void radioButton_place_CheckedChanged(object sender, EventArgs e)
        {
            _placeRemove = true;
        }

        private void radioButton_remove_CheckedChanged(object sender, EventArgs e)
        {
            _placeRemove = false;
        }

        private void mouseEnter1(int row, int col)
        {
            if (_phase == 0) //if place ship phase
            {
                if (_horzVert == 'h')
                {
                    if (_placeRemove == true)
                    {
                        setColorsHorz(row, col, _shipSize);
                    }
                }
                else
                {
                    if (_placeRemove == true)
                    {
                        setColorsVert(row, col, _shipSize);
                    }
                }
            }
        }

        private void mouseLeave1(int row, int col)
        {
            if (_phase == 0) //if place ship phase
            {
                if (_horzVert == 'h')
                {
                    resetColorsHorz(row, col, _shipSize);
                }
                else
                {
                    resetColorsVert(row, col, _shipSize);
                }
            }
        }

        private void mouseClick1(int row, int col, MouseEventArgs e)
        {
            string message = "";

            if (e.Button == MouseButtons.Right)
            {
                if (_horzVert == 'h')
                {
                    _horzVert = 'v';
                    resetColorsHorz(row, col, _shipSize);
                }
                else
                {
                    _horzVert = 'h';
                    resetColorsVert(row, col, _shipSize);
                }

                mouseEnter1(row, col);
            }
            else if(e.Button == MouseButtons.Left)
            {
                if (_placeRemove == true)
                {
                    if ((message = checkIfValidShipPlace(row, col, _shipSize)) == "")
                    {
                        label_comments.Text = _shipName + " successfully placed!";
                        if (_horzVert == 'h')
                        {
                            setShipHorz1(row, col, _shipSize, _shipName);
                        }
                        else
                        {
                            setShipVert1(row, col, _shipSize, _shipName);
                        }

                    }
                    else //invalid ship place
                    {
                        label_comments.Text = message;
                    }
                }
                else
                {
                    removeShip(row, col);
                }
            }
        }

        private void mouseClick2(int row, int col, MouseEventArgs e)
        {
            string coordinate = row.ToString() + col.ToString();
            if (e.Button == MouseButtons.Left)
            {
                if (!_targetListHistory.Contains(coordinate))
                {
                    if (_targetList.Contains(coordinate))
                    {
                        removeFromTargetListTextBox(coordinate);
                        _targetList.Remove(coordinate);
                        enablePanels2();
                        button_shoot.Enabled = false;
                        _panelMatrix2[row, col].BackColor = _waterColor;
                    }
                    else
                    {
                        if (_targetList.Count < _numberOfShips)
                        {
                            _targetList.Add(coordinate);
                            addToTargetListTextBox(coordinate);
                            _panelMatrix2[row, col].BackColor = _markField;

                            if (_targetList.Count == _numberOfShips || _targetList.Count == computerFieldsLeft())
                            {
                                button_shoot.Enabled = true;
                            }
                        }
                    }
                }
            }
        }

        private int computerFieldsLeft()
        {
            int count = 0;
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    if (_panelMatrix2[row, col].BackColor == _waterColor)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private int ownFieldsLeft()
        {
            int count = 0;
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    if (_panelMatrix1[row, col].BackColor == _waterColor || _panelMatrix1[row, col].BackColor == _shipColor)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private int ownShipFieldsLeft()
        {
            int count = 0;
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    if (_panelMatrix1[row, col].BackColor == _shipColor)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private void addToTargetListTextBox(string coordinate)
        {
            char rowLetter = ' ';
            switch (Int32.Parse(coordinate[0].ToString()))
            {
                case 0:
                    rowLetter = 'A';
                    break;
                case 1:
                    rowLetter = 'B';
                    break;
                case 2:
                    rowLetter = 'C';
                    break;
                case 3:
                    rowLetter = 'D';
                    break;
                case 4:
                    rowLetter = 'E';
                    break;
                case 5:
                    rowLetter = 'F';
                    break;
                case 6:
                    rowLetter = 'G';
                    break;
                case 7:
                    rowLetter = 'H';
                    break;
                case 8:
                    rowLetter = 'I';
                    break;
                case 9:
                    rowLetter = 'J';
                    break;
            }
            Invoke(new UpdateHandler(changeTextBoxTargetList), rowLetter.ToString() + coordinate[1].ToString() + "\r\n", true);
            //textBox_targetList.Text += rowLetter.ToString() + coordinate[1].ToString() + "\r\n";
        }

        private void removeFromTargetListTextBox(string coordinate)
        {
            char rowLetter = ' ';
            switch (Int32.Parse(coordinate[0].ToString()))
            {
                case 0:
                    rowLetter = 'A';
                    break;
                case 1:
                    rowLetter = 'B';
                    break;
                case 2:
                    rowLetter = 'C';
                    break;
                case 3:
                    rowLetter = 'D';
                    break;
                case 4:
                    rowLetter = 'E';
                    break;
                case 5:
                    rowLetter = 'F';
                    break;
                case 6:
                    rowLetter = 'G';
                    break;
                case 7:
                    rowLetter = 'H';
                    break;
                case 8:
                    rowLetter = 'I';
                    break;
                case 9:
                    rowLetter = 'J';
                    break;
            }

            bool found = false;
            int line = 0;
            string all = textBox_targetList.Text;
            string before, after;
            while (!found && line < _targetList.Count)
            {
                if (textBox_targetList.Lines[line].Equals(rowLetter.ToString() + coordinate[1].ToString()))
                {
                    found = true;
                    before = all.Substring(0, 4 * line);
                    after = all.Substring(4 * (line + 1));
                    textBox_targetList.Clear();
                    textBox_targetList.Text = before + after;
                    textBox_targetList.Update();
                }
                line++;
            }
        }

        private void removeShip(int row, int col)
        {
            string shipName;
            if (_shipMatrix1[row, col] != "")
            {
                radioButton_place.Enabled = true;
                button_battle.Enabled = false;

                shipName = _shipMatrix1[row, col];

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (_shipMatrix1[i, j].Equals(shipName))
                        {
                            _shipMatrix1[i, j] = "";
                            _panelMatrix1[i,j].BackColor = _waterColor;
                        }
                    }
                }
                
                label_comments.Text = shipName + " successfully removed";
                //get ship name

                if (shipName == "Aircraft Carrier")
                {
                    radioButton_aircraftCarrier.Enabled = true;
                    radioButton_aircraftCarrier.Checked = true;
                }
                else if (shipName == "Battleship")
                {
                    radioButton_battleship.Enabled = true;
                    radioButton_battleship.Checked = true;
                }
                else if (shipName == "Destroyer")
                {
                    radioButton_destroyer.Enabled = true;
                    radioButton_destroyer.Checked = true;
                }
                else if (shipName == "Patrol Boat")
                {
                    radioButton_patrolBoat.Enabled = true;
                    radioButton_patrolBoat.Checked = true;
                }
                else if (shipName == "Submarine")
                {
                    radioButton_submarine.Enabled = true;
                    radioButton_submarine.Checked = true;
                }
            }
        }

        private void computerSetShips()
        {
            Invoke(new UpdateHandler(changeLabelComments), "The computer is placing his ships", false);
            //label_comments.Text = "The computer is placing his ships";
            //label_comments.Update();

            Random coordinate = new Random();
            string[] shipNames = new string[5] {"Aircraft Carrier", "Battleship", "Destroyer", "Patrol Boat", "Submarine"};
            int[] shipSizes = new int[5] {5,4,3,2,3};

            for (int i = 0; i < 5; i++)
            {
                //generate coordinate
                int row = coordinate.Next(0, 10);
                int col = coordinate.Next(0, 10);
                int horzVertNumber = coordinate.Next(0, 2); //0...horizontal, 1...vertical
                if (horzVertNumber == 0)
                {
                    _horzVert = 'h';
                }
                else
                {
                    _horzVert = 'v';
                }

                while(checkIfValidShipPlace2(row, col, shipSizes[i]))
                {
                    row = coordinate.Next(0, 10);
                    col = coordinate.Next(0, 10);
                }

                if (_horzVert == 'h')//place ship horizontally
                {
                    setShipHorz2(row, col, shipSizes[i], shipNames[i]);
                }
                else
                {
                    setShipVert2(row, col, shipSizes[i], shipNames[i]);
                }
            }
            Thread.Sleep(2000);

            if (_phase == 1)
            {
                Invoke(new UpdateHandler(changeLabelComments), _playerName + " please select your target list", false);
                //label_comments.Text = _playerName + " please select your target list";
                //label_comments.Update();
                saveAsToolStripMenuItem.Enabled = true;
                loadToolStripMenuItem.Enabled = true;
                newToolStripMenuItem.Enabled = true;
            }
            else
            {
                _newThreadComputerShoot = new Thread(computerShoot);
                _newThreadComputerShoot.Start();
                //computerShoot();
            }
        }

        private void addComputerCoordinate(int row, int col)
        {
            int sleepTime = 1000;

            _computerTargetListHistory.Add(row.ToString() + col.ToString());
            _targetList.Add(row.ToString() + col.ToString());
            addToTargetListTextBox(row.ToString() + col.ToString());
            Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
            //textBox_targetList.Update();
            System.Threading.Thread.Sleep(sleepTime);
        }

        private bool lastOneRightIsMissOrBoarder(int row, int col)
        {
            int pos = 0;
            bool overBoarder = false;
            while (!overBoarder && _panelMatrix1[row, col + pos].BackColor == _hitColor)
            {
                if (col + (pos + 1) > 9 || _panelMatrix1[row, col + pos].BackColor == _missColor)
                {
                    overBoarder = true;
                }
                else
                {
                    pos++;
                }
            }
            if (_panelMatrix1[row, col + pos].BackColor == _missColor)
            {
                overBoarder = true;
            }

            return overBoarder;
        }

        private bool lastOneLeftIsMissOrBoarder(int row, int col)
        {
            int pos = 0;
            bool overBoarder = false;
            while (!overBoarder && _panelMatrix1[row, col - pos].BackColor == _hitColor)
            {
                if (col - (pos + 1) < 0 || _panelMatrix1[row, col - pos].BackColor == _missColor)
                {
                    overBoarder = true;
                }
                else
                {
                    pos++;
                }
            }
            if (_panelMatrix1[row, col - pos].BackColor == _missColor)
            {
                overBoarder = true;
            }

            return overBoarder;
        }

        private bool lastOneDownIsMissOrBoarder(int row, int col)
        {
            int pos = 0;
            bool overBoarder = false;
            while (!overBoarder && _panelMatrix1[row + pos, col].BackColor == _hitColor)
            {
                if (row + (pos + 1) > 9 || _panelMatrix1[row + pos, col].BackColor == _missColor)
                {
                    overBoarder = true;
                }
                else
                {
                    pos++;
                }
            }
            if (_panelMatrix1[row + pos, col].BackColor == _missColor)
            {
                overBoarder = true;
            }

            return overBoarder;
        }

        private bool lastOneUpIsMissOrBoarder(int row, int col)
        {
            int pos = 0;
            bool overBoarder = false;
            while (!overBoarder && _panelMatrix1[row - pos, col].BackColor == _hitColor)
            {
                if (row - (pos + 1) < 0 || _panelMatrix1[row - pos, col].BackColor == _missColor)
                {
                    overBoarder = true;
                }
                else
                {
                    pos++;
                }
            }
            if (_panelMatrix1[row - pos, col].BackColor == _missColor)
            {
                overBoarder = true;
            }

            return overBoarder;
        }

        private string getHitShipName(int row, int col)
        {
            string name = "";

            if (_shipMatrix1[row, col] == "Aircraft Carrier")
            {
                name = "Aircraft Carrier";
            }
            else if (_shipMatrix1[row, col] == "Battleship")
            {
                name = "Battleship";
            }
            else if (_shipMatrix1[row, col] == "Destroyer")
            {
                name = "Destroyer";
            }
            else if (_shipMatrix1[row, col] == "Patrol Boat")
            {
                name = "Patrol Boat";
            }
            else if (_shipMatrix1[row, col] == "Submarine")
            {
                name = "Submarine";
            }

            return name;
        }

        private List<string> getMissingCoordinates(string shipName)
        {
            List<string> coordinates = new List<string>();

            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    if (_shipMatrix1[row, col] == shipName
                        && _panelMatrix1[row, col].BackColor == _shipColor)
                    {
                        coordinates.Add(row.ToString() + col.ToString());
                    }
                }
            }

            return coordinates;
        }

        private int numberOfHitsOnShip(string shipName)
        {
            int count = 0;

            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    if (_shipMatrix1[row, col] == shipName
                        && _panelMatrix1[row, col].BackColor == _hitColor)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private void addFieldsAroundShipToTargetListHistory(string shipName)
        {
            string firstCoordinate = "";
            int row = 0, col = 0, shipSize = 0;
            bool found = false;
            char horzVert = 'h';

            //find first field of ship
            while(!found && row < 10)
            {
                col = 0;
                while(!found && col < 10)
                {
                    if (_shipMatrix1[row, col].Equals(shipName))
                    {
                        found = true;
                        firstCoordinate = row.ToString() + col.ToString();
                    }
                    col++;
                }
                row++;
            }

            row = Int32.Parse(firstCoordinate[0].ToString());
            col = Int32.Parse(firstCoordinate[1].ToString());

            //get shipsize
            if (shipName.Equals("Aircraft Carrier"))
            {
                shipSize = 5;
            }
            else if (shipName.Equals("Battleship"))
            {
                shipSize = 4;
            }
            else if (shipName.Equals("Destroyer"))
            {
                shipSize = 3;
            }
            else if (shipName.Equals("Patrol Boat"))
            {
                shipSize = 2;
            }
            else if (shipName.Equals("Submarine"))
            {
                shipSize = 3;
            }

            if (row == 0)
            {
                //checking if the ship is horizontal or vertical
                if (_shipMatrix1[row+1, col].Equals(shipName))
                {
                    horzVert = 'v';
                }

                if (col == 0)
                {
                    if (horzVert == 'h')
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            for (int j = 0; j < (shipSize + 1); j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < (shipSize + 1); i++)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                }
                else if (col == 9)
                {
                    //ship can only be vertical
                    for (int i = 0; i < (shipSize + 1); i++)
                    {
                        for (int j = 8; j < 10; j++)
                        {
                            if (!_shipMatrix1[i, j].Equals(shipName) &&
                                !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                            {
                                _computerTargetListHistory.Add(i.ToString() + j.ToString());
                            }
                        }
                    }
                }
                else//coordinate is in first row
                {
                    if (horzVert == 'h')
                    {
                        if ((col + shipSize) < 10)//this is one more than the ship
                        {
                            for (int i = 0; i < 2; i++)
                            {
                                for (int j = col - 1; j < col + (shipSize + 1); j++)
                                {
                                    if (!_shipMatrix1[i, j].Equals(shipName) &&
                                        !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                    {
                                        _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                    }
                                }
                            }
                        }
                        else//last field of the ship is just at the border
                        {
                            for (int i = 0; i < 2; i++)
                            {
                                for (int j = col - 1; j < col + (shipSize); j++)
                                {
                                    if (!_shipMatrix1[i, j].Equals(shipName) &&
                                        !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                    {
                                        _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < (shipSize + 1); i++)
                        {
                            for (int j = col - 1; j <= (col + 1); j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                }
            }
            else if (row == 9)//ship can only be horizontal => don't have to check
            {
                if (col == 0)
                {
                    for (int i = 8; i < 10; i++)
                    {
                        for (int j = 0; j < (shipSize + 1); j++)
                        {
                            if (!_shipMatrix1[i, j].Equals(shipName) &&
                                !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                            {
                                _computerTargetListHistory.Add(i.ToString() + j.ToString());
                            }
                        }
                    }
                }
                else//coordinate is in the last row
                {
                    if ((col + shipSize) < 10)//this is one more than the ship
                    {
                        for (int i = 8; i < 10; i++)
                        {
                            for (int j = col - 1; j < col + (shipSize + 1); j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                    else//last field of the ship is just at the border
                    {
                        for (int i = 8; i < 10; i++)
                        {
                            for (int j = col - 1; j < col + (shipSize); j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                }
            }
            else if (col == 0)//coordinate is in the first colomn
            {
                //checking if the ship is horizontal or vertical
                if (_shipMatrix1[row + 1, col].Equals(shipName))
                {
                    horzVert = 'v';
                }

                if (horzVert == 'h')
                {
                    for (int i = row - 1; i < row + 2; i++)
                    {
                        for (int j = 0; j < (shipSize + 1); j++)
                        {
                            if (!_shipMatrix1[i, j].Equals(shipName) &&
                                !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                            {
                                _computerTargetListHistory.Add(i.ToString() + j.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if ((row + shipSize) < 10)//this is one more than the ship
                    {
                        for (int i = row - 1; i < row + (shipSize + 1); i++)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                    else//last field of the ship is just at the border
                    {
                        for (int i = row - 1; i < row + (shipSize); i++)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                }
            }
            else if (col == 9)//coordinate is in the last colomn; ship can only be vertical => don't have to check
            {
                if ((row + shipSize) < 10)//this is one more than the ship
                {
                    for (int i = row - 1; i < row + (shipSize + 1); i++)
                    {
                        for (int j = 8; j < 10; j++)
                        {
                            if (!_shipMatrix1[i, j].Equals(shipName) &&
                                !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                            {
                                _computerTargetListHistory.Add(i.ToString() + j.ToString());
                            }
                        }
                    }
                }
                else//last field of the ship is just at the border
                {
                    for (int i = row - 1; i < row + (shipSize); i++)
                    {
                        for (int j = 8; j < 10; j++)
                        {
                            if (!_shipMatrix1[i, j].Equals(shipName) &&
                                !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                            {
                                _computerTargetListHistory.Add(i.ToString() + j.ToString());
                            }
                        }
                    }
                }
            }
            else//coordinate is anywhere else
            {
                //checking if the ship is horizontal or vertical
                if (_shipMatrix1[row + 1, col].Equals(shipName))
                {
                    horzVert = 'v';
                }

                if (horzVert == 'h')
                {
                    if ((col + shipSize) < 10)//this is one more than the ship
                    {
                        for (int i = row - 1; i < row + 2; i++)
                        {
                            for (int j = col - 1; j < col + (shipSize + 1); j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                    else//last field of the ship is just at the border
                    {
                        for (int i = row - 1; i < row + 2; i++)
                        {
                            for (int j = col - 1; j < col + (shipSize); j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                }
                else//ship is vertical
                {
                    if ((row + shipSize) < 10)//this is one more than the ship
                    {
                        for (int i = row - 1; i < row + (shipSize + 1); i++)
                        {
                            for (int j = col - 1; j < col + 2; j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                    else//last field of the ship is just at the border
                    {
                        for (int i = row - 1; i < row + (shipSize); i++)
                        {
                            for (int j = col - 1; j < col + 2; j++)
                            {
                                if (!_shipMatrix1[i, j].Equals(shipName) &&
                                    !_computerTargetListHistory.Contains(i.ToString() + j.ToString()))
                                {
                                    _computerTargetListHistory.Add(i.ToString() + j.ToString());
                                }
                            }
                        }
                    }
                }
            }
        }

        private int computerShootRandomly(int shots)
        {
            Random rand = new Random();
            int sleepTime = 1000;
            int row = rand.Next(0, 10);
            int col = rand.Next(0, 10);

            while (shots > 0)
            {
                if (shots < ownFieldsLeft())
                {
                    while (_computerTargetListHistory.Contains(row.ToString() + col.ToString()))
                    {
                        row = rand.Next(0, 10);
                        col = rand.Next(0, 10);
                    }
                    _computerTargetListHistory.Add(row.ToString() + col.ToString());
                    _targetList.Add(row.ToString() + col.ToString());
                    addToTargetListTextBox(row.ToString() + col.ToString());
                    Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
                    //textBox_targetList.Update();
                    System.Threading.Thread.Sleep(sleepTime);
                }
                shots--;
            }

            return shots;
        }

        public void computerShoot()
        {
            Random rand = new Random();
            int row = 0, col = 0;
            int sleepTime = 1000;
            bool done = false;

            Invoke(new UpdateHandlerMenuItmes(changeMenuItems), false, false, false);
            //saveAsToolStripMenuItem.Enabled = false;
            //loadToolStripMenuItem.Enabled = false;
            //newToolStripMenuItem.Enabled = false;

            Invoke(new UpdateHandler(changeLabelComments), "The computer is populating the target list ...", false);
            //label_comments.Text = "The computer is populating the target list ...";
            //label_comments.Update();     

            if (_difficulty == '1')//easy
            {
                row = rand.Next(0, 10);
                col = rand.Next(0, 10);

                for (int i = 0; i < _computerNumberOfShips; i++)
                {
                    if (i < ownFieldsLeft())
                    {
                        while (_computerTargetListHistory.Contains(row.ToString() + col.ToString()))
                        {
                            row = rand.Next(0, 10);
                            col = rand.Next(0, 10);
                        }
                        _computerTargetListHistory.Add(row.ToString() + col.ToString());
                        _targetList.Add(row.ToString() + col.ToString());
                        addToTargetListTextBox(row.ToString() + col.ToString());
                        Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
                        //textBox_targetList.Update();
                        System.Threading.Thread.Sleep(sleepTime);
                    }
                }
            }
            else if (_difficulty == '2')//hard
            {
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (_panelMatrix1[i, j].BackColor == _destroyedColor)
                        {
                            _computerHitHistory.Remove(i.ToString() + j.ToString());
                        }
                    }
                }

                if (_computerHitHistory.Count == 0) //check if it did hit anything
                {
                    row = rand.Next(0, 10);
                    col = rand.Next(0, 10);

                    for (int i = 0; i < _computerNumberOfShips; i++)
                    {
                        if (i < ownFieldsLeft())
                        {
                            while (_computerTargetListHistory.Contains(row.ToString() + col.ToString()))
                            {
                                row = rand.Next(0, 10);
                                col = rand.Next(0, 10);
                            }
                            _computerTargetListHistory.Add(row.ToString() + col.ToString());
                            _targetList.Add(row.ToString() + col.ToString());
                            addToTargetListTextBox(row.ToString() + col.ToString());
                            //textBox_targetList.Update();
                            System.Threading.Thread.Sleep(sleepTime);
                        }
                    }
                }
                else
                {
                    int shots;
                    string searchname;
                    bool goToNextShip = false;
                    int hitHistoryCount = 1;
                    shots = _computerNumberOfShips;

                    while (shots > 0 && _computerHitHistory.Count > 0)
                    {
                        if (!goToNextShip)
                        {
                            row = Int32.Parse(_computerHitHistory[0][0].ToString());
                            col = Int32.Parse(_computerHitHistory[0][1].ToString());
                        }

                        searchname = getHitShipName(row, col);

                        if (numberOfHitsOnShip(searchname) >= 2)
                        {
                            List<string> coordinates = getMissingCoordinates(searchname);
                            //added
                            addFieldsAroundShipToTargetListHistory(searchname);

                            while (shots > 0 && coordinates.Count > 0)
                            {
                                addComputerCoordinate(Int32.Parse(coordinates[0][0].ToString()), Int32.Parse(coordinates[0][1].ToString()));
                                shots--;
                                coordinates.RemoveAt(0);
                            }

                            //if there are shots left, it means the ship was destroyed => remove it from the hitlist
                            if (shots > 0)
                            {
                                _computerHitHistory.RemoveAt(0);
                            }
                        }
                        else
                        {
                            if (row == 0)
                            {
                                if (col == 0)
                                {
                                    if (shots > 0 && !lastOneDownIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row + 1).ToString() + col.ToString()))
                                    {
                                        addComputerCoordinate(row + 1, col); // shoot down
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneRightIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col + 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col + 1); // shoot right
                                        shots--;
                                    }
                                    else
                                    {
                                        //added
                                        addFieldsAroundShipToTargetListHistory(searchname);
                                        if (_computerHitHistory.Count > hitHistoryCount)
                                        {
                                            row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                            col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                            goToNextShip = true;
                                            hitHistoryCount++;
                                        }
                                        else
                                        {
                                            shots = computerShootRandomly(shots);
                                        }
                                    }
                                }
                                else if (col == 9)
                                {
                                    if (shots > 0 && !lastOneDownIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row + 1).ToString() + col.ToString()))
                                    {
                                        addComputerCoordinate(row + 1, col); // shoot down
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneLeftIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col - 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col - 1); // shoot left
                                        shots--;
                                    }
                                    else
                                    {
                                        //added
                                        addFieldsAroundShipToTargetListHistory(searchname);
                                        if (_computerHitHistory.Count > hitHistoryCount)
                                        {
                                            row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                            col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                            goToNextShip = true;
                                            hitHistoryCount++;
                                        }
                                        else
                                        {
                                            shots = computerShootRandomly(shots);
                                        }
                                    }
                                }
                                //ship is in the first row
                                else
                                {
                                    if (shots > 0 && !lastOneDownIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row + 1).ToString() + col.ToString()))
                                    {
                                        addComputerCoordinate(row + 1, col); // shoot down
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneRightIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col + 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col + 1); // shoot right
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneLeftIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col - 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col - 1); // shoot left
                                        shots--;
                                    }
                                    else
                                    {
                                        //added
                                        addFieldsAroundShipToTargetListHistory(searchname);
                                        if (_computerHitHistory.Count > hitHistoryCount)
                                        {
                                            row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                            col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                            goToNextShip = true;
                                            hitHistoryCount++;
                                        }
                                        else
                                        {
                                            shots = computerShootRandomly(shots);
                                        }
                                    }
                                }
                            }
                            else if (row == 9)
                            {
                                if (col == 0)
                                {
                                    if (shots > 0 && !lastOneUpIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains((row - 1).ToString() + col.ToString()))
                                    {
                                        addComputerCoordinate(row - 1, col); // shoot up
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneRightIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col + 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col + 1); // shoot right
                                        shots--;
                                    }
                                    else
                                    {
                                        //added
                                        addFieldsAroundShipToTargetListHistory(searchname);
                                        if (_computerHitHistory.Count > hitHistoryCount)
                                        {
                                            row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                            col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                            goToNextShip = true;
                                            hitHistoryCount++;
                                        }
                                        else
                                        {
                                            shots = computerShootRandomly(shots);
                                        }
                                    }
                                }
                                else if (col == 9)
                                {
                                    if (shots > 0 && !lastOneUpIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains((row - 1).ToString() + col.ToString()))
                                    {
                                        addComputerCoordinate(row - 1, col); // shoot up
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneLeftIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col - 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col - 1); // shoot left
                                        shots--;
                                    }
                                    else
                                    {
                                        //added
                                        addFieldsAroundShipToTargetListHistory(searchname);
                                        if (_computerHitHistory.Count > hitHistoryCount)
                                        {
                                            row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                            col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                            goToNextShip = true;
                                            hitHistoryCount++;
                                        }
                                        else
                                        {
                                            shots = computerShootRandomly(shots);
                                        }
                                    }
                                }
                                //ship is in the last row
                                else
                                {
                                    if (shots > 0 && !lastOneUpIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains((row - 1).ToString() + col.ToString()))
                                    {
                                        addComputerCoordinate(row - 1, col); // shoot up
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneRightIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col + 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col + 1); // shoot right
                                        shots--;
                                    }
                                    if (shots > 0 && !lastOneLeftIsMissOrBoarder(row, col)
                                        && !_computerTargetListHistory.Contains(row.ToString() + (col - 1).ToString()))
                                    {
                                        addComputerCoordinate(row, col - 1); // shoot left
                                        shots--;
                                    }
                                    else
                                    {
                                        //added
                                        addFieldsAroundShipToTargetListHistory(searchname);
                                        if (_computerHitHistory.Count > hitHistoryCount)
                                        {
                                            row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                            col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                            goToNextShip = true;
                                            hitHistoryCount++;
                                        }
                                        else
                                        {
                                            shots = computerShootRandomly(shots);
                                        }
                                    }
                                }
                            }
                            else if (col == 0)
                            {
                                if (shots > 0 && !lastOneDownIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row + 1).ToString() + col.ToString()))
                                {
                                    addComputerCoordinate(row + 1, col); // shoot down
                                    shots--;
                                }
                                if (shots > 0 && !lastOneUpIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row - 1).ToString() + col.ToString()))
                                {
                                    addComputerCoordinate(row - 1, col); // shoot up
                                    shots--;
                                }
                                if (shots > 0 && !lastOneRightIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains(row.ToString() + (col + 1).ToString()))
                                {
                                    addComputerCoordinate(row, col + 1); // shoot right
                                    shots--;
                                }
                                else
                                {
                                    //added
                                    addFieldsAroundShipToTargetListHistory(searchname);
                                    if (_computerHitHistory.Count > hitHistoryCount)
                                    {
                                        row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                        col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                        goToNextShip = true;
                                        hitHistoryCount++;
                                    }
                                    else
                                    {
                                        shots = computerShootRandomly(shots);
                                    }
                                }
                            }
                            else if (col == 9)
                            {
                                if (shots > 0 && !lastOneDownIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row + 1).ToString() + col.ToString()))
                                {
                                    addComputerCoordinate(row + 1, col); // shoot down
                                    shots--;
                                }
                                if (shots > 0 && !lastOneUpIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row - 1).ToString() + col.ToString()))
                                {
                                    addComputerCoordinate(row - 1, col); // shoot up
                                    shots--;
                                }
                                if (shots > 0 && !lastOneLeftIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains(row.ToString() + (col - 1).ToString()))
                                {
                                    addComputerCoordinate(row, col - 1); // shoot left
                                    shots--;
                                }
                                else
                                {
                                    //added
                                    addFieldsAroundShipToTargetListHistory(searchname);
                                    if (_computerHitHistory.Count > hitHistoryCount)
                                    {
                                        row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                        col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                        goToNextShip = true;
                                        hitHistoryCount++;
                                    }
                                    else
                                    {
                                        shots = computerShootRandomly(shots);
                                    }
                                }
                            }
                            //anywhere else
                            else
                            {
                                if (shots > 0 && !lastOneDownIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row + 1).ToString() + col.ToString()))
                                {
                                    addComputerCoordinate(row + 1, col); // shoot down
                                    shots--;
                                }
                                if (shots > 0 && !lastOneUpIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains((row - 1).ToString() + col.ToString()))
                                {
                                    addComputerCoordinate(row - 1, col); // shoot up
                                    shots--;
                                }
                                if (shots > 0 && !lastOneRightIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains(row.ToString() + (col + 1).ToString()))
                                {
                                    addComputerCoordinate(row, col + 1); // shoot right
                                    shots--;
                                }
                                if (shots > 0 && !lastOneLeftIsMissOrBoarder(row, col)
                                    && !_computerTargetListHistory.Contains(row.ToString() + (col - 1).ToString()))
                                {
                                    addComputerCoordinate(row, col - 1); // shoot left
                                    shots--;
                                }
                                else
                                {
                                    //added
                                    addFieldsAroundShipToTargetListHistory(searchname);
                                    if (_computerHitHistory.Count > hitHistoryCount)
                                    {
                                        row = Int32.Parse(_computerHitHistory[hitHistoryCount][0].ToString());
                                        col = Int32.Parse(_computerHitHistory[hitHistoryCount][1].ToString());
                                        goToNextShip = true;
                                        hitHistoryCount++;
                                    }
                                    else
                                    {
                                        shots = computerShootRandomly(shots);
                                    }
                                }
                            }
                        }
                    }
                    //if there are no other ships left that have a hit then shoot randomly
                    if (shots > 0)
                    {
                        computerShootRandomly(shots);
                    }
                }
            }
            else//impossible
            {
                bool stop = false;
                row = rand.Next(0, 10);
                col = rand.Next(0, 10);
                int shotNumber;

                for (int i = 0; i < _computerNumberOfShips; i++)
                {
                    //decides if the shot is randomly or not
                    //removed it, so when computer has only 1 ship left => shoot randomly (makes it more beatable)
                    //if (_computerNumberOfShips == 1)
                    //{
                        //shotNumber = rand.Next(0, 2);//at least 50% hit
                    //}
                    //else
                    //{
                        //5 ships = at least 80% hit
                        //4 ships = at least 75% hit
                        //3 ships = at least 66% hit
                        //2 ships = at least 50% hit
                        //1 ship = randomly
                        shotNumber = rand.Next(0, _computerNumberOfShips);
                    //}
                    if (i < ownFieldsLeft())
                    {
                        if (i == shotNumber)
                        {
                            //shoot randomly to give player a chance
                            while (_computerTargetListHistory.Contains(row.ToString() + col.ToString()))
                            {
                                row = rand.Next(0, 10);
                                col = rand.Next(0, 10);
                            }
                            _computerTargetListHistory.Add(row.ToString() + col.ToString());
                            _targetList.Add(row.ToString() + col.ToString());
                            addToTargetListTextBox(row.ToString() + col.ToString());
                            Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
                            //textBox_targetList.Update();
                            System.Threading.Thread.Sleep(sleepTime);
                        }
                        else
                        {
                            while (!stop && (_computerTargetListHistory.Contains(row.ToString() + col.ToString()) || _shipMatrix1[row, col] == ""))
                            {
                                row = rand.Next(0, 10);
                                col = rand.Next(0, 10);

                                if (ownShipFieldsLeft() <= i && !_computerTargetListHistory.Contains(row.ToString() + col.ToString()))
                                {
                                    stop = true;
                                }
                            }
                            _computerTargetListHistory.Add(row.ToString() + col.ToString());
                            _targetList.Add(row.ToString() + col.ToString());
                            addToTargetListTextBox(row.ToString() + col.ToString());
                            Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
                            //textBox_targetList.Update();
                            System.Threading.Thread.Sleep(sleepTime);
                            stop = false;
                        }
                    }
                }
            }

            Invoke(new UpdateHandler(changeLabelComments), "The computer is shooting at your fleet...", false);
            //label_comments.Text = "The computer is shooting at your fleet...";
            //label_comments.Update();
            //textBox_history.Text += _break;
            Invoke(new UpdateHandler(changeTextBoxHistory), _break, true);

            //check the fields
            while (_targetList.Count != 0 && !done)
            {
                string field = _targetList[0];

                if (_shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "")
                {
                    historyUpdater(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), "The computer", 0, "");
                    _panelMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())].BackColor = _missColor;
                    Invoke(new UpdateHandlerUpdatePanel(updatePanel1), Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
                    //_panelMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())].Update();
                }
                else //hit
                {
                    //for hard only; add only different ships to the hitlist => max. 5 entries
                    if (_difficulty == '2')
                    {
                        if (_computerHitHistory.Count > 0)
                        {
                            int count = 0;
                            for (int i = 0; i < _computerHitHistory.Count; i++)
                            {
                                if (_shipMatrix1[Int32.Parse(_computerHitHistory[i][0].ToString()), Int32.Parse(_computerHitHistory[i][1].ToString())]
                                    == _shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())])
                                {
                                    count++;
                                }
                            }
                            if (count == 0)
                            {
                                _computerHitHistory.Add(field);
                            }
                        }
                        else
                        {
                            _computerHitHistory.Add(field);
                        }
                    }

                    //check first if the ship was destroyed
                    if (_shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Aircraft Carrier")
                    {
                        helpShoot2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), "The computer", "Aircraft Carrier", --_lifeAircraftCarrier);
                    }
                    else if (_shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Battleship")
                    {
                        helpShoot2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), "The computer", "Battleship", --_lifeBattleship);
                    }
                    else if (_shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Destroyer")
                    {
                        helpShoot2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), "The computer", "Destroyer", --_lifeDestroyer);
                    }
                    else if (_shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Patrol Boat")
                    {
                        helpShoot2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), "The computer", "Patrol Boat", --_lifePatrolBoat);
                    }
                    else if (_shipMatrix1[Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString())] == "Submarine")
                    {
                        helpShoot2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), "The computer", "Submarine", --_lifeSubmarine);
                    }
                }
                _targetList.RemoveAt(0);

                //removing the first line from the target list textbox
                string all = textBox_targetList.Text;
                string rest = all.Substring(textBox_targetList.Lines[0].Length + 2);
                Invoke(new UpdateHandlerUpdate(clearTextBoxTargetList));
                Invoke(new UpdateHandler(changeTextBoxTargetList), rest, false);
                //textBox_targetList.Clear();
                //textBox_targetList.Text = rest;

                //updating textboxes
                Invoke(new UpdateHandlerUpdate(updateTextBoxTargetList));
                //textBox_targetList.Update();
                //textBox_history.Update();

                //wait a little bit
                System.Threading.Thread.Sleep(sleepTime);
     
                if (_numberOfShips == 0)
                {
                    done = true;
                }
            }
            if (!done)
            {
                //change controls
                _phase = 1;
                Invoke(new UpdateHandlerUpdate(enablePanels2));
                //enablePanels2();

                Invoke(new UpdateHandler(changeLabelComments), _playerName + " please select your target list", false);
                //label_comments.Text = _playerName + " please select your target list";
                //label_comments.Update();
                Invoke(new UpdateHandlerMenuItmes(changeMenuItems), true, true, true);
                //saveAsToolStripMenuItem.Enabled = true;
                //loadToolStripMenuItem.Enabled = true;
                //newToolStripMenuItem.Enabled = true;
            }
            else
            {
                Invoke(new UpdateHandlerMenuItmes(changeMenuItems), false, true, true);
                //saveAsToolStripMenuItem.Enabled = false;
                //loadToolStripMenuItem.Enabled = true;
                //newToolStripMenuItem.Enabled = true;

                //update records
                updateRecord("loss");
                _finishedGame = true;

                endOfGameWindow(false);
            }
        }

        private void loadProfile()
        {
            Record searchRecord = new Record(_playerName);
            if (_records.Contains(searchRecord))
            {
                int index = _records.IndexOf(searchRecord);
                _profile = _records[index];
            }
            else //create record
            {
                _profile = new Record(_playerName);
                _records.Add(_profile);
            }
        }

        private void updateRecord(string result)
        {
            if (result.Equals("win"))
            {
                _profile.getDifficulty(_difficulty).addWin(getTimeDifference(_startTime, DateTime.Now.ToLocalTime()));
            }
            else
            {
                _profile.getDifficulty(_difficulty).addLoss(getTimeDifference(_startTime, DateTime.Now.ToLocalTime()));
            }
        }

        private DateTime getTimeDifference(DateTime startTime, DateTime endTime)
        {
            if (_fileLoaded)
            {
                return DateTime.MaxValue;
            }
            TimeSpan span = endTime.Subtract(startTime);
            if (span.Hours > 0)
            {
                return new DateTime(1, 12, 31, 0, 59, 59);
            }
            return new DateTime(1, 12, 31, 0, span.Minutes, span.Seconds);
        }

        private void loadRecordsFromFile()
        {
            try
            {
                Stream stream = File.Open("records.log", FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                _records = (List<Record>)formatter.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                try
                {
                    Stream stream = File.Create("records.log");
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, _records);
                    stream.Close();
                }
                catch { }
            }
        }

        private void saveRecordsToFile()
        {
            try
            {
                Stream stream = File.Create("records.log");
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, _records);
                stream.Close();
            }
            catch { }
        }

        public static int ownShipFieldsHit()
        {
            int count = 0;
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    if (form2._panelMatrix1[row, col].BackColor == form2._hitColor ||
                        form2._panelMatrix1[row, col].BackColor == form2._destroyedColor)
                    {
                        if (form2._shipMatrix1[row, col] == "Aircraft Carrier")
                        {
                            count += 1;
                        }
                        else if (form2._shipMatrix1[row, col] == "Battleship")
                        {
                            count += 2;
                        }
                        else if (form2._shipMatrix1[row, col] == "Destroyer")
                        {
                            count += 3;
                        }
                        else if (form2._shipMatrix1[row, col] == "Submarine")
                        {
                            count += 3;
                        }
                        else if (form2._shipMatrix1[row, col] == "Patrol Boat")
                        {
                            count += 5;
                        }
                    }
                }
            }

            return count;
        }

        public static int calculateScore()
        {
            //scoring system:
            //each destroyed ship gives 12 points
            //destroyed ship bonus: patrol boat = 2, submarine & destroyer = 3 each, battleship = 4, aircraft carrier = 7
            //hit ship points per field: patrol boat = 5, submarine & destroyer = 3 each, battleship = 2, aircraft carrier = 1
            //maximum score = 60, min score = 6
            int maxScore = 60, computerScore = 0;

            //calculate computer score
            computerScore = ownShipFieldsHit();
            if (form2._lifeAircraftCarrier <= 0)
            {
                computerScore += 7;
            }
            if (form2._lifeBattleship <= 0)
            {
                computerScore += 4;
            }
            if (form2._lifeDestroyer <= 0)
            {
                computerScore += 3;
            }
            if (form2._lifeSubmarine <= 0)
            {
                computerScore += 3;
            }
            if (form2._lifePatrolBoat <= 0)
            {
                computerScore += 2;
            }

            //subtract score
            return (maxScore - computerScore);
        }

        private void endOfGameWindow(bool wonLost)
        {
            Form10 infoWindow = new Form10();
            infoWindow.form2 = this;
            infoWindow.setWonLost(wonLost);
            infoWindow.setGameTime(getTimeDifference(_startTime, DateTime.Now.ToLocalTime()));
            infoWindow.ShowDialog();
        }

        //user field panels
        //
        //panel1
        //
        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            string field = "00";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            string field = "00";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "00";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel2
        //
        private void panel2_MouseEnter(object sender, EventArgs e)
        {
            string field = "01";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel2_MouseLeave(object sender, EventArgs e)
        {
            string field = "01";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "01";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel3
        //
        private void panel3_MouseEnter(object sender, EventArgs e)
        {
            string field = "02";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel3_MouseLeave(object sender, EventArgs e)
        {
            string field = "02";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "02";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel4
        //
        private void panel4_MouseEnter(object sender, EventArgs e)
        {
            string field = "03";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel4_MouseLeave(object sender, EventArgs e)
        {
            string field = "03";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "03";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel5
        //
        private void panel5_MouseEnter(object sender, EventArgs e)
        {
            string field = "04";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel5_MouseLeave(object sender, EventArgs e)
        {
            string field = "04";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel5_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "04";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel6
        //
        private void panel6_MouseEnter(object sender, EventArgs e)
        {
            string field = "05";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel6_MouseLeave(object sender, EventArgs e)
        {
            string field = "05";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel6_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "05";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel7
        //
        private void panel7_MouseEnter(object sender, EventArgs e)
        {
            string field = "06";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel7_MouseLeave(object sender, EventArgs e)
        {
            string field = "06";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel7_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "06";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel8
        //
        private void panel8_MouseEnter(object sender, EventArgs e)
        {
            string field = "07";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel8_MouseLeave(object sender, EventArgs e)
        {
            string field = "07";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel8_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "07";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel9
        //
        private void panel9_MouseEnter(object sender, EventArgs e)
        {
            string field = "08";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel9_MouseLeave(object sender, EventArgs e)
        {
            string field = "08";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel9_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "08";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel10
        //
        private void panel10_MouseEnter(object sender, EventArgs e)
        {
            string field = "09";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel10_MouseLeave(object sender, EventArgs e)
        {
            string field = "09";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel10_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "09";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel11
        //
        private void panel11_MouseEnter(object sender, EventArgs e)
        {
            string field = "10";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel11_MouseLeave(object sender, EventArgs e)
        {
            string field = "10";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel11_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "10";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel12
        //
        private void panel12_MouseEnter(object sender, EventArgs e)
        {
            string field = "11";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel12_MouseLeave(object sender, EventArgs e)
        {
            string field = "11";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel12_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "11";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel13
        //
        private void panel13_MouseEnter(object sender, EventArgs e)
        {
            string field = "12";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel13_MouseLeave(object sender, EventArgs e)
        {
            string field = "12";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel13_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "12";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel14
        //
        private void panel14_MouseEnter(object sender, EventArgs e)
        {
            string field = "13";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel14_MouseLeave(object sender, EventArgs e)
        {
            string field = "13";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel14_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "13";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel15
        //
        private void panel15_MouseEnter(object sender, EventArgs e)
        {
            string field = "14";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel15_MouseLeave(object sender, EventArgs e)
        {
            string field = "14";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel15_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "14";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel16
        //
        private void panel16_MouseEnter(object sender, EventArgs e)
        {
            string field = "15";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel16_MouseLeave(object sender, EventArgs e)
        {
            string field = "15";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel16_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "15";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel17
        //
        private void panel17_MouseEnter(object sender, EventArgs e)
        {
            string field = "16";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel17_MouseLeave(object sender, EventArgs e)
        {
            string field = "16";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel17_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "16";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel18
        //
        private void panel18_MouseEnter(object sender, EventArgs e)
        {
            string field = "17";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel18_MouseLeave(object sender, EventArgs e)
        {
            string field = "17";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel18_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "17";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel19
        //
        private void panel19_MouseEnter(object sender, EventArgs e)
        {
            string field = "18";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel19_MouseLeave(object sender, EventArgs e)
        {
            string field = "18";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel19_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "18";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel20
        //
        private void panel20_MouseEnter(object sender, EventArgs e)
        {
            string field = "19";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel20_MouseLeave(object sender, EventArgs e)
        {
            string field = "19";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel20_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "19";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel21
        //
        private void panel21_MouseEnter(object sender, EventArgs e)
        {
            string field = "20";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel21_MouseLeave(object sender, EventArgs e)
        {
            string field = "20";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel21_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "20";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel22
        //
        private void panel22_MouseEnter(object sender, EventArgs e)
        {
            string field = "21";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel22_MouseLeave(object sender, EventArgs e)
        {
            string field = "21";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel22_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "21";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel23
        //
        private void panel23_MouseEnter(object sender, EventArgs e)
        {
            string field = "22";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel23_MouseLeave(object sender, EventArgs e)
        {
            string field = "22";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel23_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "22";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel24
        //
        private void panel24_MouseEnter(object sender, EventArgs e)
        {
            string field = "23";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel24_MouseLeave(object sender, EventArgs e)
        {
            string field = "23";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel24_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "23";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel25
        //
        private void panel25_MouseEnter(object sender, EventArgs e)
        {
            string field = "24";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel25_MouseLeave(object sender, EventArgs e)
        {
            string field = "24";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel25_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "24";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel26
        //
        private void panel26_MouseEnter(object sender, EventArgs e)
        {
            string field = "25";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel26_MouseLeave(object sender, EventArgs e)
        {
            string field = "25";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel26_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "25";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel27
        //
        private void panel27_MouseEnter(object sender, EventArgs e)
        {
            string field = "26";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel27_MouseLeave(object sender, EventArgs e)
        {
            string field = "26";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel27_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "26";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel28
        //
        private void panel28_MouseEnter(object sender, EventArgs e)
        {
            string field = "27";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel28_MouseLeave(object sender, EventArgs e)
        {
            string field = "27";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel28_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "27";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel29
        //
        private void panel29_MouseEnter(object sender, EventArgs e)
        {
            string field = "28";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel29_MouseLeave(object sender, EventArgs e)
        {
            string field = "28";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel29_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "28";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }//
        //panel30
        //
        private void panel30_MouseEnter(object sender, EventArgs e)
        {
            string field = "29";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel30_MouseLeave(object sender, EventArgs e)
        {
            string field = "29";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel30_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "29";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel31
        //
        private void panel31_MouseEnter(object sender, EventArgs e)
        {
            string field = "30";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel31_MouseLeave(object sender, EventArgs e)
        {
            string field = "30";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel31_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "30";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel32
        //
        private void panel32_MouseEnter(object sender, EventArgs e)
        {
            string field = "31";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel32_MouseLeave(object sender, EventArgs e)
        {
            string field = "31";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel32_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "31";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel33
        //
        private void panel33_MouseEnter(object sender, EventArgs e)
        {
            string field = "32";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel33_MouseLeave(object sender, EventArgs e)
        {
            string field = "32";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel33_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "32";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel34
        //
        private void panel34_MouseEnter(object sender, EventArgs e)
        {
            string field = "33";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel34_MouseLeave(object sender, EventArgs e)
        {
            string field = "33";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel34_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "33";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel35
        //
        private void panel35_MouseEnter(object sender, EventArgs e)
        {
            string field = "34";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel35_MouseLeave(object sender, EventArgs e)
        {
            string field = "34";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel35_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "34";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel36
        //
        private void panel36_MouseEnter(object sender, EventArgs e)
        {
            string field = "35";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel36_MouseLeave(object sender, EventArgs e)
        {
            string field = "35";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel36_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "35";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel37
        //
        private void panel37_MouseEnter(object sender, EventArgs e)
        {
            string field = "36";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel37_MouseLeave(object sender, EventArgs e)
        {
            string field = "36";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel37_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "36";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel38
        //
        private void panel38_MouseEnter(object sender, EventArgs e)
        {
            string field = "37";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel38_MouseLeave(object sender, EventArgs e)
        {
            string field = "37";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel38_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "37";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel39
        //
        private void panel39_MouseEnter(object sender, EventArgs e)
        {
            string field = "38";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel39_MouseLeave(object sender, EventArgs e)
        {
            string field = "38";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel39_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "38";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel40
        //
        private void panel40_MouseEnter(object sender, EventArgs e)
        {
            string field = "39";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel40_MouseLeave(object sender, EventArgs e)
        {
            string field = "39";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel40_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "39";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel41
        //
        private void panel41_MouseEnter(object sender, EventArgs e)
        {
            string field = "40";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel41_MouseLeave(object sender, EventArgs e)
        {
            string field = "40";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel41_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "40";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel42
        //
        private void panel42_MouseEnter(object sender, EventArgs e)
        {
            string field = "41";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel42_MouseLeave(object sender, EventArgs e)
        {
            string field = "41";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel42_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "41";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel43
        //
        private void panel43_MouseEnter(object sender, EventArgs e)
        {
            string field = "42";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel43_MouseLeave(object sender, EventArgs e)
        {
            string field = "42";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel43_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "42";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel44
        //
        private void panel44_MouseEnter(object sender, EventArgs e)
        {
            string field = "43";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel44_MouseLeave(object sender, EventArgs e)
        {
            string field = "43";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel44_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "43";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel45
        //
        private void panel45_MouseEnter(object sender, EventArgs e)
        {
            string field = "44";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel45_MouseLeave(object sender, EventArgs e)
        {
            string field = "44";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel45_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "44";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel46
        //
        private void panel46_MouseEnter(object sender, EventArgs e)
        {
            string field = "45";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel46_MouseLeave(object sender, EventArgs e)
        {
            string field = "45";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel46_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "45";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel47
        //
        private void panel47_MouseEnter(object sender, EventArgs e)
        {
            string field = "46";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel47_MouseLeave(object sender, EventArgs e)
        {
            string field = "46";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel47_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "46";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel48
        //
        private void panel48_MouseEnter(object sender, EventArgs e)
        {
            string field = "47";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel48_MouseLeave(object sender, EventArgs e)
        {
            string field = "47";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel48_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "47";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel49
        //
        private void panel49_MouseEnter(object sender, EventArgs e)
        {
            string field = "48";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel49_MouseLeave(object sender, EventArgs e)
        {
            string field = "48";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel49_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "48";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel50
        //
        private void panel50_MouseEnter(object sender, EventArgs e)
        {
            string field = "49";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel50_MouseLeave(object sender, EventArgs e)
        {
            string field = "49";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel50_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "49";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel51
        //
        private void panel51_MouseEnter(object sender, EventArgs e)
        {
            string field = "50";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel51_MouseLeave(object sender, EventArgs e)
        {
            string field = "50";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel51_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "50";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel52
        //
        private void panel52_MouseEnter(object sender, EventArgs e)
        {
            string field = "51";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel52_MouseLeave(object sender, EventArgs e)
        {
            string field = "51";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel52_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "51";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel53
        //
        private void panel53_MouseEnter(object sender, EventArgs e)
        {
            string field = "52";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel53_MouseLeave(object sender, EventArgs e)
        {
            string field = "52";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel53_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "52";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel54
        //
        private void panel54_MouseEnter(object sender, EventArgs e)
        {
            string field = "53";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel54_MouseLeave(object sender, EventArgs e)
        {
            string field = "53";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel54_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "53";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel55
        //
        private void panel55_MouseEnter(object sender, EventArgs e)
        {
            string field = "54";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel55_MouseLeave(object sender, EventArgs e)
        {
            string field = "54";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel55_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "54";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel56
        //
        private void panel56_MouseEnter(object sender, EventArgs e)
        {
            string field = "55";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel56_MouseLeave(object sender, EventArgs e)
        {
            string field = "55";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel56_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "55";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel57
        //
        private void panel57_MouseEnter(object sender, EventArgs e)
        {
            string field = "56";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel57_MouseLeave(object sender, EventArgs e)
        {
            string field = "56";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel57_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "56";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel58
        //
        private void panel58_MouseEnter(object sender, EventArgs e)
        {
            string field = "57";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel58_MouseLeave(object sender, EventArgs e)
        {
            string field = "57";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel58_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "57";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel59
        //
        private void panel59_MouseEnter(object sender, EventArgs e)
        {
            string field = "58";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel59_MouseLeave(object sender, EventArgs e)
        {
            string field = "58";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel59_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "58";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel60
        //
        private void panel60_MouseEnter(object sender, EventArgs e)
        {
            string field = "59";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel60_MouseLeave(object sender, EventArgs e)
        {
            string field = "59";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel60_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "59";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel61
        //
        private void panel61_MouseEnter(object sender, EventArgs e)
        {
            string field = "60";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel61_MouseLeave(object sender, EventArgs e)
        {
            string field = "60";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel61_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "60";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel62
        //
        private void panel62_MouseEnter(object sender, EventArgs e)
        {
            string field = "61";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel62_MouseLeave(object sender, EventArgs e)
        {
            string field = "61";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel62_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "61";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel63
        //
        private void panel63_MouseEnter(object sender, EventArgs e)
        {
            string field = "62";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel63_MouseLeave(object sender, EventArgs e)
        {
            string field = "62";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel63_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "62";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel64
        //
        private void panel64_MouseEnter(object sender, EventArgs e)
        {
            string field = "63";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel64_MouseLeave(object sender, EventArgs e)
        {
            string field = "63";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel64_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "63";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel65
        //
        private void panel65_MouseEnter(object sender, EventArgs e)
        {
            string field = "64";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel65_MouseLeave(object sender, EventArgs e)
        {
            string field = "64";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel65_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "64";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel66
        //
        private void panel66_MouseEnter(object sender, EventArgs e)
        {
            string field = "65";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel66_MouseLeave(object sender, EventArgs e)
        {
            string field = "65";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel66_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "65";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel67
        //
        private void panel67_MouseEnter(object sender, EventArgs e)
        {
            string field = "66";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel67_MouseLeave(object sender, EventArgs e)
        {
            string field = "66";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel67_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "66";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel68
        //
        private void panel68_MouseEnter(object sender, EventArgs e)
        {
            string field = "67";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel68_MouseLeave(object sender, EventArgs e)
        {
            string field = "67";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel68_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "67";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel69
        //
        private void panel69_MouseEnter(object sender, EventArgs e)
        {
            string field = "68";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel69_MouseLeave(object sender, EventArgs e)
        {
            string field = "68";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel69_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "68";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel70
        //
        private void panel70_MouseEnter(object sender, EventArgs e)
        {
            string field = "69";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel70_MouseLeave(object sender, EventArgs e)
        {
            string field = "69";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel70_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "69";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel71
        //
        private void panel71_MouseEnter(object sender, EventArgs e)
        {
            string field = "70";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel71_MouseLeave(object sender, EventArgs e)
        {
            string field = "70";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel71_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "70";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel72
        //
        private void panel72_MouseEnter(object sender, EventArgs e)
        {
            string field = "71";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel72_MouseLeave(object sender, EventArgs e)
        {
            string field = "71";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel72_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "71";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel73
        //
        private void panel73_MouseEnter(object sender, EventArgs e)
        {
            string field = "72";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel73_MouseLeave(object sender, EventArgs e)
        {
            string field = "72";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel73_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "72";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel74
        //
        private void panel74_MouseEnter(object sender, EventArgs e)
        {
            string field = "73";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel74_MouseLeave(object sender, EventArgs e)
        {
            string field = "73";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel74_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "73";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel75
        //
        private void panel75_MouseEnter(object sender, EventArgs e)
        {
            string field = "74";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel75_MouseLeave(object sender, EventArgs e)
        {
            string field = "74";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel75_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "74";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel76
        //
        private void panel76_MouseEnter(object sender, EventArgs e)
        {
            string field = "75";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel76_MouseLeave(object sender, EventArgs e)
        {
            string field = "75";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel76_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "75";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel77
        //
        private void panel77_MouseEnter(object sender, EventArgs e)
        {
            string field = "76";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel77_MouseLeave(object sender, EventArgs e)
        {
            string field = "76";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel77_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "76";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel78
        //
        private void panel78_MouseEnter(object sender, EventArgs e)
        {
            string field = "77";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel78_MouseLeave(object sender, EventArgs e)
        {
            string field = "77";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel78_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "77";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel79
        //
        private void panel79_MouseEnter(object sender, EventArgs e)
        {
            string field = "78";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel79_MouseLeave(object sender, EventArgs e)
        {
            string field = "78";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel79_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "78";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel80
        //
        private void panel80_MouseEnter(object sender, EventArgs e)
        {
            string field = "79";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel80_MouseLeave(object sender, EventArgs e)
        {
            string field = "79";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel80_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "79";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel81
        //
        private void panel81_MouseEnter(object sender, EventArgs e)
        {
            string field = "80";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel81_MouseLeave(object sender, EventArgs e)
        {
            string field = "80";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel81_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "80";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel82
        //
        private void panel82_MouseEnter(object sender, EventArgs e)
        {
            string field = "81";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel82_MouseLeave(object sender, EventArgs e)
        {
            string field = "81";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel82_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "81";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel83
        //
        private void panel83_MouseEnter(object sender, EventArgs e)
        {
            string field = "82";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel83_MouseLeave(object sender, EventArgs e)
        {
            string field = "82";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel83_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "82";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel84
        //
        private void panel84_MouseEnter(object sender, EventArgs e)
        {
            string field = "83";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel84_MouseLeave(object sender, EventArgs e)
        {
            string field = "83";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel84_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "83";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel85
        //
        private void panel85_MouseEnter(object sender, EventArgs e)
        {
            string field = "84";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel85_MouseLeave(object sender, EventArgs e)
        {
            string field = "84";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel85_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "84";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel86
        //
        private void panel86_MouseEnter(object sender, EventArgs e)
        {
            string field = "85";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel86_MouseLeave(object sender, EventArgs e)
        {
            string field = "85";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel86_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "85";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel87
        //
        private void panel87_MouseEnter(object sender, EventArgs e)
        {
            string field = "86";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel87_MouseLeave(object sender, EventArgs e)
        {
            string field = "86";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel87_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "86";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel88
        //
        private void panel88_MouseEnter(object sender, EventArgs e)
        {
            string field = "87";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel88_MouseLeave(object sender, EventArgs e)
        {
            string field = "87";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel88_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "87";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel89
        //
        private void panel89_MouseEnter(object sender, EventArgs e)
        {
            string field = "88";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel89_MouseLeave(object sender, EventArgs e)
        {
            string field = "88";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel89_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "88";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel90
        //
        private void panel90_MouseEnter(object sender, EventArgs e)
        {
            string field = "89";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel90_MouseLeave(object sender, EventArgs e)
        {
            string field = "89";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel90_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "89";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel91
        //
        private void panel91_MouseEnter(object sender, EventArgs e)
        {
            string field = "90";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel91_MouseLeave(object sender, EventArgs e)
        {
            string field = "90";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel91_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "90";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel92
        //
        private void panel92_MouseEnter(object sender, EventArgs e)
        {
            string field = "91";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel92_MouseLeave(object sender, EventArgs e)
        {
            string field = "91";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel92_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "91";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel93
        //
        private void panel93_MouseEnter(object sender, EventArgs e)
        {
            string field = "92";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel93_MouseLeave(object sender, EventArgs e)
        {
            string field = "92";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel93_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "92";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel94
        //
        private void panel94_MouseEnter(object sender, EventArgs e)
        {
            string field = "93";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel94_MouseLeave(object sender, EventArgs e)
        {
            string field = "93";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel94_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "93";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel95
        //
        private void panel95_MouseEnter(object sender, EventArgs e)
        {
            string field = "94";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }
        
        private void panel95_MouseLeave(object sender, EventArgs e)
        {
            string field = "94";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel95_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "94";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel96
        //
        private void panel96_MouseEnter(object sender, EventArgs e)
        {
            string field = "95";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel96_MouseLeave(object sender, EventArgs e)
        {
            string field = "95";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel96_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "95";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel97
        //
        private void panel97_MouseEnter(object sender, EventArgs e)
        {
            string field = "96";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel97_MouseLeave(object sender, EventArgs e)
        {
            string field = "96";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel97_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "96";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel98
        //
        private void panel98_MouseEnter(object sender, EventArgs e)
        {
            string field = "97";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel98_MouseLeave(object sender, EventArgs e)
        {
            string field = "97";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel98_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "97";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel99
        //
        private void panel99_MouseEnter(object sender, EventArgs e)
        {
            string field = "98";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel99_MouseLeave(object sender, EventArgs e)
        {
            string field = "98";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel99_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "98";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }
        //
        //panel100
        //
        private void panel100_MouseEnter(object sender, EventArgs e)
        {
            string field = "99";
            mouseEnter1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel100_MouseLeave(object sender, EventArgs e)
        {
            string field = "99";
            mouseLeave1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()));
        }

        private void panel100_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "99";
            mouseClick1(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }


        //
        //
        //computer field panels
        //
        //


        private void panel101_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "00";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel102_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "01";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel103_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "02";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel104_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "03";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel105_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "04";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel106_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "05";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel107_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "06";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel108_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "07";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel109_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "08";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel110_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "09";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel111_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "10";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel112_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "11";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel113_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "12";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel114_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "13";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel115_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "14";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel116_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "15";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel117_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "16";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel118_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "17";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel119_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "18";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel120_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "19";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel121_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "20";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel122_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "21";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel123_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "22";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel124_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "23";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel125_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "24";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel126_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "25";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel127_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "26";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel128_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "27";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel129_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "28";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel130_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "29";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel131_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "30";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel132_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "31";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel133_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "32";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel134_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "33";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel135_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "34";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel136_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "35";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel137_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "36";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel138_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "37";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel139_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "38";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel140_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "39";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel141_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "40";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel142_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "41";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel143_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "42";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel144_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "43";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel145_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "44";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel146_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "45";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel147_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "46";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel148_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "47";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel149_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "48";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel150_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "49";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel151_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "50";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel152_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "51";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel153_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "52";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel154_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "53";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel155_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "54";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel156_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "55";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel157_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "56";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel158_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "57";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel159_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "58";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel160_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "59";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel161_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "60";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel162_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "61";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel163_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "62";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel164_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "63";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel165_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "64";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel166_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "65";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel167_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "66";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel168_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "67";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel169_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "68";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel170_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "69";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel171_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "70";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel172_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "71";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel173_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "72";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel174_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "73";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel175_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "74";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel176_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "75";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel177_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "76";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel178_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "77";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel179_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "78";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel180_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "79";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel181_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "80";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel182_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "81";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel183_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "82";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel184_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "83";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel185_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "84";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel186_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "85";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel187_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "86";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel188_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "87";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel189_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "88";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel190_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "89";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel191_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "90";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel192_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "91";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel193_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "92";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel194_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "93";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel195_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "94";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel196_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "95";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel197_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "96";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel198_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "97";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel199_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "98";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void panel200_MouseClick(object sender, MouseEventArgs e)
        {
            string field = "99";
            mouseClick2(Int32.Parse(field[0].ToString()), Int32.Parse(field[1].ToString()), e);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //settings for the save box
            saveFileDialog_game.Filter = "Ship Destroyer File (.shd)|*.shd";
            saveFileDialog_game.CheckPathExists = true;
            saveFileDialog_game.AddExtension = true;
            saveFileDialog_game.CreatePrompt = false;
            saveFileDialog_game.DefaultExt = "shd";
            saveFileDialog_game.OverwritePrompt = true;
            saveFileDialog_game.RestoreDirectory = false;
            saveFileDialog_game.ValidateNames = true;
            saveFileDialog_game.ShowDialog();
        }

        private void saveFileDialog_game_FileOk(object sender, CancelEventArgs e)
        {          
            string fileName = saveFileDialog_game.FileName;
            string extension = fileName.Substring(fileName.Length - 4);

            if (extension != ".shd")
            {
                fileName += ".shd";
            }

            if (fileName != "")
            {
                try
                {
                    FileInformation info = new FileInformation();
                    Color color = new Color();

                    info._playerName = _playerName;
                    info._difficulty = _difficulty;
                    info.copyTargetListHistory(_targetListHistory);
                    info.copyComputerHitHistory(_computerHitHistory);
                    info.copyComputerTargetListHistory(_computerTargetListHistory);
                    info._phase = _phase;
                    info._numberOfShips = _numberOfShips;
                    info._computerNumberOfShips = _computerNumberOfShips;
                    info._lifeAircraftCarrier = _lifeAircraftCarrier;
                    info._lifeBattleship = _lifeBattleship;
                    info._lifeDestroyer = _lifeDestroyer;
                    info._lifePatrolBoat = _lifePatrolBoat;
                    info._lifeSubmarine = _lifeSubmarine;
                    info._computerLifeAircraftCarrier = _computerLifeAircraftCarrier;
                    info._computerLifeBattleship = _computerLifeBattleship;
                    info._computerLifeDestroyer = _computerLifeDestroyer;
                    info._computerLifePatrolBoat = _computerLifePatrolBoat;
                    info._computerLifeSubmarine = _computerLifeSubmarine;
                    info.copyShipMatrix1(_shipMatrix1);
                    info.copyShipMatrix2(_shipMatrix2);
                    info._historyTextBox = textBox_history.Text;
                    for (int row = 0; row < 10; row++)
                    {
                        for (int col = 0; col < 10; col++)
                        {
                            color = _panelMatrix1[row, col].BackColor;
                            if (color == _waterColor)
                            {
                                info.copyPanelMatrix1(row, col, "water");
                            }
                            else if (color == _hitColor)
                            {
                                info.copyPanelMatrix1(row, col, "hit");
                            }
                            else if (color == _missColor)
                            {
                                info.copyPanelMatrix1(row, col, "miss");
                            }
                            else if (color == _shipColor)
                            {
                                info.copyPanelMatrix1(row, col, "ship");
                            }
                            else if (color == _destroyedColor)
                            {
                                info.copyPanelMatrix1(row, col, "destroyed");
                            }

                            // for panel 2
                            color = _panelMatrix2[row, col].BackColor;
                            if (color == _waterColor || color == _markField)
                            {
                                info.copyPanelMatrix2(row, col, "water");
                            }
                            else if (color == _hitColor)
                            {
                                info.copyPanelMatrix2(row, col, "hit");
                            }
                            else if (color == _missColor)
                            {
                                info.copyPanelMatrix2(row, col, "miss");
                            }
                            else if (color == _destroyedColor)
                            {
                                info.copyPanelMatrix2(row, col, "destroyed");
                            }
                        }
                    }
                    //order: water, ship, miss, hit, mark, destroyed
                    info._colors[0] = _waterColor.ToArgb();
                    info._colors[1] = _shipColor.ToArgb();
                    info._colors[2] = _missColor.ToArgb();
                    info._colors[3] = _hitColor.ToArgb();
                    info._colors[4] = _markField.ToArgb();
                    info._colors[5] = _destroyedColor.ToArgb();

                    Stream stream = File.Create(fileName);
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, info);
                    stream.Close();
                    _fileSaved = true;
                }
                catch
                {
                    MessageBox.Show("Error occured while saving the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //settings for the open box
            openFileDialog_game.Filter = "Ship Destroyer File (.shd)|*.shd";
            openFileDialog_game.CheckPathExists = true;
            openFileDialog_game.AddExtension = true;
            openFileDialog_game.DefaultExt = "shd";
            openFileDialog_game.RestoreDirectory = false;
            openFileDialog_game.ValidateNames = true;
            openFileDialog_game.ShowDialog();
        }

        private void openFileDialog_game_FileOk(object sender, CancelEventArgs e)
        {
            string fileName = openFileDialog_game.FileName;
            saveAsToolStripMenuItem.Enabled = true;
            loadToolStripMenuItem.Enabled = true;
            newToolStripMenuItem.Enabled = true;

            if (fileName != "")
            {
                try
                {
                    Stream stream = File.Open(fileName, FileMode.Open);
                    BinaryFormatter formatter = new BinaryFormatter();
                    FileInformation info = (FileInformation)formatter.Deserialize(stream);
                    stream.Close();
                    string color = "";
                    _targetListHistory = new List<string>();
                    _computerHitHistory = new List<string>();
                    _computerTargetListHistory = new List<string>();

                    _playerName = info._playerName;
                    _difficulty = info._difficulty;
                    _targetListHistory = info._targetListHistory;
                    _computerHitHistory = info._computerHitHistory;
                    _computerTargetListHistory = info._computerTargetListHistory;
                    _phase = info._phase;
                    _numberOfShips = info._numberOfShips;
                    _computerNumberOfShips = info._computerNumberOfShips;
                    _lifeAircraftCarrier = info._lifeAircraftCarrier;
                    _lifeBattleship = info._lifeBattleship;
                    _lifeDestroyer = info._lifeDestroyer;
                    _lifePatrolBoat = info._lifePatrolBoat;
                    _lifeSubmarine = info._lifeSubmarine;
                    _computerLifeAircraftCarrier = info._computerLifeAircraftCarrier;
                    _computerLifeBattleship = info._computerLifeBattleship;
                    _computerLifeDestroyer = info._computerLifeDestroyer;
                    _computerLifePatrolBoat = info._computerLifePatrolBoat;
                    _computerLifeSubmarine = info._computerLifeSubmarine;
                    _shipMatrix1 = info._shipMatrix1;
                    _shipMatrix2 = info._shipMatrix2;
                    textBox_history.Text = info._historyTextBox;
                    //order: water, ship, miss, hit, mark, destroyed
                    _waterColor = Color.FromArgb(info._colors[0]);
                    _shipColor = Color.FromArgb(info._colors[1]);
                    _missColor = Color.FromArgb(info._colors[2]);
                    _hitColor = Color.FromArgb(info._colors[3]);
                    _markField = Color.FromArgb(info._colors[4]);
                    _destroyedColor = Color.FromArgb(info._colors[5]);
                    for (int row = 0; row < 10; row++)
                    {
                        for (int col = 0; col < 10; col++)
                        {
                            color = info.getPanelMatrix1(row, col);
                            if (color.Equals("water"))
                            {
                                _panelMatrix1[row, col].BackColor = _waterColor;
                            }
                            else if (color.Equals("hit"))
                            {
                                _panelMatrix1[row, col].BackColor = _hitColor;
                            }
                            else if (color.Equals("miss"))
                            {
                                _panelMatrix1[row, col].BackColor = _missColor;
                            }
                            else if (color.Equals("ship"))
                            {
                                _panelMatrix1[row, col].BackColor = _shipColor;
                            }
                            else if (color.Equals("destroyed"))
                            {
                                _panelMatrix1[row, col].BackColor = _destroyedColor;
                            }

                            // for panel 2
                            color = info.getPanelMatrix2(row, col);
                            if (color.Equals("water"))
                            {
                                _panelMatrix2[row, col].BackColor = _waterColor;
                            }
                            else if (color.Equals("hit"))
                            {
                                _panelMatrix2[row, col].BackColor = _hitColor;
                            }
                            else if (color.Equals("miss"))
                            {
                                _panelMatrix2[row, col].BackColor = _missColor;
                            }
                            else if (color.Equals("destroyed"))
                            {
                                _panelMatrix2[row, col].BackColor = _destroyedColor;
                            }
                        }
                    }

                    //everything is loaded
                    loadProfile();
                    _fileLoaded = true;
                    _finishedGame = false;
                    activateTextBoxHistory();
                    textBox_history.SelectionStart = textBox_history.Text.Length;
                    textBox_history.ScrollToCaret();
                    label_playerName.Text = _playerName;
                    _targetList.Clear();
                    textBox_targetList.Clear();
                    
                    if (_phase == 1) //player is shooter
                    {
                        disablePanels1();
                        enablePanels2();

                        label_comments.Text = _playerName + " please select your target list";
                    }
                    else //computer is shooter
                    {
                        disablePanels1();
                        disablePanels2();
                        _newThreadComputerShoot = new Thread(computerShoot);
                        _newThreadComputerShoot.Start();
                        //computerShoot();
                    }
                }
                catch
                {
                    MessageBox.Show("Error occured while loading the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5();
            form5.ShowDialog();
        }

        private void colorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form6 colors = new Form6();
            colors.form2 = this;
            colors.ShowDialog();
        }

        private void recordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form7 record = new Form7();
            record.form2 = this;
            record.ShowDialog();
        }
    }
}