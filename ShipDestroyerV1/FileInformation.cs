using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ShipDestroyerV1
{
    [Serializable]
    class FileInformation : ISerializable
    {
        public string _playerName;
        public char _difficulty;
        public List<string> _targetListHistory;
        public List<string> _computerHitHistory;
        public List<string> _computerTargetListHistory;
        public int _phase;
        public int _numberOfShips;
        public int _computerNumberOfShips;
        public int _lifeAircraftCarrier;
        public int _lifeBattleship;
        public int _lifeDestroyer;
        public int _lifePatrolBoat;
        public int _lifeSubmarine;
        public int _computerLifeAircraftCarrier;
        public int _computerLifeBattleship;
        public int _computerLifeDestroyer;
        public int _computerLifePatrolBoat;
        public int _computerLifeSubmarine;
        public string[,] _shipMatrix1;
        public string[,] _shipMatrix2;
        public string[,] _panelMatrix1;
        public string[,] _panelMatrix2;
        public string _historyTextBox;
        public int[] _colors;
       
        public FileInformation()
        {
            _targetListHistory = new List<string>();
            _computerHitHistory = new List<string>();
            _computerTargetListHistory = new List<string>();
            _shipMatrix1 = new string[10,10];
            _shipMatrix2 = new string[10,10];
            _panelMatrix1 = new string[10,10];
            _panelMatrix2 = new string[10,10];
            _colors = new int[6];
        }

        public void copyTargetListHistory(List<string> list){
            foreach(string coordinate in list)
            {
                _targetListHistory.Add(coordinate);
            }
        }

        public void copyComputerHitHistory(List<string> list){
            foreach(string coordinate in list)
            {
                _computerHitHistory.Add(coordinate);
            }
        }

        public void copyComputerTargetListHistory(List<string> list){
            foreach(string coordinate in list)
            {
                _computerTargetListHistory.Add(coordinate);
            }
        }

        public void copyShipMatrix1(string[,] matrix)
        {
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _shipMatrix1[row,col] = matrix[row, col];
                }
            }
        }

        public void copyShipMatrix2(string[,] matrix)
        {
            for (int row = 0; row < 10; row++)
            {
                for (int col = 0; col < 10; col++)
                {
                    _shipMatrix2[row,col] = matrix[row, col];
                }
            }
        }
        
        public void copyPanelMatrix1(int row, int col, string color)
        {
            _panelMatrix1[row, col] = color;
        }

        public void copyPanelMatrix2(int row, int col, string color)
        {
            _panelMatrix2[row, col] = color;
        }

        public string getPanelMatrix1(int row, int col)
        {
            return _panelMatrix1[row, col];
        }

        public string getPanelMatrix2(int row, int col)
        {
            return _panelMatrix2[row, col];
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("playerName", _playerName);
            info.AddValue("difficulty", _difficulty);
            info.AddValue("targetListHistory", _targetListHistory);
            info.AddValue("computerHitHistory", _computerHitHistory);
            info.AddValue("computerTargetListHistory", _computerTargetListHistory);
            info.AddValue("phase", _phase);
            info.AddValue("numberOfShips", _numberOfShips);
            info.AddValue("computerNumberOfShips", _computerNumberOfShips);
            info.AddValue("lifeAircraftCarrier", _lifeAircraftCarrier);
            info.AddValue("lifeBattleship", _lifeBattleship);
            info.AddValue("lifeDestroyer", _lifeDestroyer);
            info.AddValue("lifePatrolBoat", _lifePatrolBoat);
            info.AddValue("lifeSubmarine", _lifeSubmarine);
            info.AddValue("computerLifeAircraftCarrier", _computerLifeAircraftCarrier);
            info.AddValue("computerLifeBattleship", _computerLifeBattleship);
            info.AddValue("computerLifeDestroyer", _computerLifeDestroyer);
            info.AddValue("computerLifePatrolBoat", _computerLifePatrolBoat);
            info.AddValue("computerLifeSubmarine", _computerLifeSubmarine);
            info.AddValue("shipMatrix1", _shipMatrix1);
            info.AddValue("shipMatrix2", _shipMatrix2);
            info.AddValue("panelMatrix1", _panelMatrix1);
            info.AddValue("panelMatrix2", _panelMatrix2);
            info.AddValue("historyTextBox", _historyTextBox);
            info.AddValue("colors", _colors);
        }

        public FileInformation(SerializationInfo info, StreamingContext ctxt)
        {
            _playerName = (string)info.GetValue("playerName", typeof(string));
            _difficulty = (char)info.GetValue("difficulty", typeof(char));
            _targetListHistory = (List<string>)info.GetValue("targetListHistory", typeof(List<string>));
            _computerHitHistory = (List<string>)info.GetValue("computerHitHistory", typeof(List<string>));
            _computerTargetListHistory = (List<string>)info.GetValue("computerTargetListHistory", typeof(List<string>));
            _phase = (int)info.GetValue("phase", typeof(int));
            _numberOfShips = (int)info.GetValue("numberOfShips", typeof(int));
            _computerNumberOfShips = (int)info.GetValue("computerNumberOfShips", typeof(int));
            _lifeAircraftCarrier = (int)info.GetValue("lifeAircraftCarrier", typeof(int));
            _lifeBattleship = (int)info.GetValue("lifeBattleship", typeof(int));
            _lifeDestroyer = (int)info.GetValue("lifeDestroyer", typeof(int));
            _lifePatrolBoat = (int)info.GetValue("lifePatrolBoat", typeof(int));
            _lifeSubmarine = (int)info.GetValue("lifeSubmarine", typeof(int));
            _computerLifeAircraftCarrier = (int)info.GetValue("computerLifeAircraftCarrier", typeof(int));
            _computerLifeBattleship = (int)info.GetValue("computerLifeBattleship", typeof(int));
            _computerLifeDestroyer = (int)info.GetValue("computerLifeDestroyer", typeof(int));
            _computerLifePatrolBoat = (int)info.GetValue("computerLifePatrolBoat", typeof(int));
            _computerLifeSubmarine = (int)info.GetValue("computerLifeSubmarine", typeof(int));
            _shipMatrix1 = (string[,])info.GetValue("shipMatrix1", typeof(string[,]));
            _shipMatrix2 = (string[,])info.GetValue("shipMatrix2", typeof(string[,]));
            _panelMatrix1 = (string[,])info.GetValue("panelMatrix1", typeof(string[,]));
            _panelMatrix2 = (string[,])info.GetValue("panelMatrix2", typeof(string[,]));
            _historyTextBox = (string)info.GetValue("historyTextBox", typeof(string));
            _colors = (int[])info.GetValue("colors", typeof(int[]));
        }

        
    }
}
