using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ShipDestroyerV1
{
    public partial class Form10 : Form
    {
        public Form2 form2;
        private bool _wonLost;
        private DateTime _gameTime;

        public Form10()
        {
            InitializeComponent();
            _wonLost = false;
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            int score = Form2.calculateScore();
            if (_wonLost)
            {
                this.Text = "Victory!";
                label_comment.Text = "You won this game!";
                if (form2._fileLoaded)
                {
                    label_gameTime.Text = "Not available";
                }
                else
                {
                    label_gameTime.Text = formatString(_gameTime);
                }
                label_score.Text = score.ToString();
            }
            else
            {
                this.Text = "Defeat";
                label_comment.Text = "You lost this game!";
                if (form2._fileLoaded)
                {
                    label_gameTime.Text = "Not available";
                }
                else
                {
                    label_gameTime.Text = formatString(_gameTime);
                }
                label_score.Text = score.ToString();
            }
        }

        public void setGameTime(DateTime gameTime)
        {
            _gameTime = gameTime;
        }

        public void setWonLost(bool wonLost)
        {
            _wonLost = wonLost;
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string formatString(DateTime time)
        {
            string minutes = "", seconds = "";

            if (time.Minute.ToString().Length == 1)
            {
                minutes = "0" + time.Minute.ToString();
            }
            else
            {
                minutes = time.Minute.ToString();
            }
            minutes += "m";

            if (time.Second.ToString().Length == 1)
            {
                seconds = "0" + time.Second.ToString();
            }
            else
            {
                seconds = time.Second.ToString();
            }
            seconds += "s";

            return minutes + seconds;
        }
    }
}