using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ShipDestroyerV1
{
    public partial class Form8 : Form
    {
        public Form7 form7;

        public Form8()
        {
            InitializeComponent();
        }

        private void button_yes_Click(object sender, EventArgs e)
        {
            form7._clearResult = true;
            this.Close();
        }

        private void button_no_Click(object sender, EventArgs e)
        {
            form7._clearResult = false;
            this.Close();
        }
    }
}